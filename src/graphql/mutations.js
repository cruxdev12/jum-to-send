// eslint-disable
// this is an auto generated file. This will be overwritten

export const createClient = `mutation CreateClient($input: CreateClientInput!) {
  createClient(input: $input) {
    id
    clientId
    email
    companyName
    companyAddress
    companyPhoneNumber
    firstName
    lastName
    jobTitle
    directPhoneOrExt
    hearAboutUs
    federatedIdentity
    hasUploadLogo
  }
}
`;
export const updateClient = `mutation UpdateClient($input: UpdateClientInput!) {
  updateClient(input: $input) {
    id
    clientId
    email
    companyName
    companyAddress
    companyPhoneNumber
    firstName
    lastName
    jobTitle
    directPhoneOrExt
    hearAboutUs
    federatedIdentity
    hasUploadLogo
  }
}
`;
export const deleteClient = `mutation DeleteClient($input: DeleteClientInput!) {
  deleteClient(input: $input) {
    id
    clientId
    email
    companyName
    companyAddress
    companyPhoneNumber
    firstName
    lastName
    jobTitle
    directPhoneOrExt
    hearAboutUs
    federatedIdentity
    hasUploadLogo
  }
}
`;
export const createCampaignPost = `mutation CreateCampaignPost($input: CreateCampaignPostInput!) {
  createCampaignPost(input: $input) {
    id
    campaignName
    startDate
    duration
    approvedDate
    campaignLocation
    carsToBeDeployed
    CAMPAIGNSTATUS
  }
}
`;
export const updateCampaignPost = `mutation UpdateCampaignPost($input: UpdateCampaignPostInput!) {
  updateCampaignPost(input: $input) {
    id
    campaignName
    startDate
    duration
    approvedDate
    campaignLocation
    carsToBeDeployed
    CAMPAIGNSTATUS
  }
}
`;
export const deleteCampaignPost = `mutation DeleteCampaignPost($input: DeleteCampaignPostInput!) {
  deleteCampaignPost(input: $input) {
    id
    campaignName
    startDate
    duration
    approvedDate
    campaignLocation
    carsToBeDeployed
    CAMPAIGNSTATUS
  }
}
`;
export const createCampaignLocation = `mutation CreateCampaignLocation($input: CreateCampaignLocationInput!) {
  createCampaignLocation(input: $input) {
    id
    locationName
    state
    country
    postcodes
    mapImageUrl
    estReach
    population
    estNumberVehicles
    regDrivers
  }
}
`;
export const updateCampaignLocation = `mutation UpdateCampaignLocation($input: UpdateCampaignLocationInput!) {
  updateCampaignLocation(input: $input) {
    id
    locationName
    state
    country
    postcodes
    mapImageUrl
    estReach
    population
    estNumberVehicles
    regDrivers
  }
}
`;
export const deleteCampaignLocation = `mutation DeleteCampaignLocation($input: DeleteCampaignLocationInput!) {
  deleteCampaignLocation(input: $input) {
    id
    locationName
    state
    country
    postcodes
    mapImageUrl
    estReach
    population
    estNumberVehicles
    regDrivers
  }
}
`;
export const createCampaignPostContent = `mutation CreateCampaignPostContent($input: CreateCampaignPostContentInput!) {
  createCampaignPostContent(input: $input) {
    id
    CONTENTSTATUS
    contentStatusDeclineCause
    artworkFile
    artworkComment
    createdAt
  }
}
`;
export const updateCampaignPostContent = `mutation UpdateCampaignPostContent($input: UpdateCampaignPostContentInput!) {
  updateCampaignPostContent(input: $input) {
    id
    CONTENTSTATUS
    contentStatusDeclineCause
    artworkFile
    artworkComment
    createdAt
  }
}
`;
export const deleteCampaignPostContent = `mutation DeleteCampaignPostContent($input: DeleteCampaignPostContentInput!) {
  deleteCampaignPostContent(input: $input) {
    id
    CONTENTSTATUS
    contentStatusDeclineCause
    artworkFile
    artworkComment
    createdAt
  }
}
`;
