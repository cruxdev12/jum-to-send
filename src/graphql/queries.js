// eslint-disable
// this is an auto generated file. This will be overwritten

export const getClient = `query GetClient($id: ID!) {
  getClient(id: $id) {
    id
    clientId
    email
    companyName
    companyAddress
    companyPhoneNumber
    firstName
    lastName
    jobTitle
    directPhoneOrExt
    hearAboutUs
    federatedIdentity
    hasUploadLogo
  }
}
`;
export const listClients = `query ListClients(
  $filter: ModelClientFilterInput
  $limit: Int
  $nextToken: String
) {
  listClients(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      clientId
      email
      companyName
      companyAddress
      companyPhoneNumber
      firstName
      lastName
      jobTitle
      directPhoneOrExt
      hearAboutUs
      federatedIdentity
      hasUploadLogo
    }
    nextToken
  }
}
`;
export const getCampaignPost = `query GetCampaignPost($id: ID!) {
  getCampaignPost(id: $id) {
    id
    campaignName
    startDate
    duration
    approvedDate
    campaignLocation
    carsToBeDeployed
    CAMPAIGNSTATUS
  }
}
`;
export const listCampaignPosts = `query ListCampaignPosts(
  $filter: ModelCampaignPostFilterInput
  $limit: Int
  $nextToken: String
) {
  listCampaignPosts(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      campaignName
      startDate
      duration
      approvedDate
      campaignLocation
      carsToBeDeployed
      CAMPAIGNSTATUS
    }
    nextToken
  }
}
`;
export const getCampaignLocation = `query GetCampaignLocation($id: ID!) {
  getCampaignLocation(id: $id) {
    id
    locationName
    state
    country
    postcodes
    mapImageUrl
    estReach
    population
    estNumberVehicles
    regDrivers
  }
}
`;
export const listCampaignLocations = `query ListCampaignLocations(
  $filter: ModelCampaignLocationFilterInput
  $limit: Int
  $nextToken: String
) {
  listCampaignLocations(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      locationName
      state
      country
      postcodes
      mapImageUrl
      estReach
      population
      estNumberVehicles
      regDrivers
    }
    nextToken
  }
}
`;
export const getCampaignPostContent = `query GetCampaignPostContent($id: ID!) {
  getCampaignPostContent(id: $id) {
    id
    CONTENTSTATUS
    contentStatusDeclineCause
    artworkFile
    artworkComment
    createdAt
  }
}
`;
export const listCampaignPostContents = `query ListCampaignPostContents(
  $filter: ModelCampaignPostContentFilterInput
  $limit: Int
  $nextToken: String
) {
  listCampaignPostContents(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      CONTENTSTATUS
      contentStatusDeclineCause
      artworkFile
      artworkComment
      createdAt
    }
    nextToken
  }
}
`;
