// eslint-disable
// this is an auto generated file. This will be overwritten

export const onCreateClient = `subscription OnCreateClient {
  onCreateClient {
    id
    clientId
    email
    companyName
    companyAddress
    companyPhoneNumber
    firstName
    lastName
    jobTitle
    directPhoneOrExt
    hearAboutUs
    federatedIdentity
    hasUploadLogo
  }
}
`;
export const onUpdateClient = `subscription OnUpdateClient {
  onUpdateClient {
    id
    clientId
    email
    companyName
    companyAddress
    companyPhoneNumber
    firstName
    lastName
    jobTitle
    directPhoneOrExt
    hearAboutUs
    federatedIdentity
    hasUploadLogo
  }
}
`;
export const onDeleteClient = `subscription OnDeleteClient {
  onDeleteClient {
    id
    clientId
    email
    companyName
    companyAddress
    companyPhoneNumber
    firstName
    lastName
    jobTitle
    directPhoneOrExt
    hearAboutUs
    federatedIdentity
    hasUploadLogo
  }
}
`;
export const onCreateCampaignPost = `subscription OnCreateCampaignPost {
  onCreateCampaignPost {
    id
    campaignName
    startDate
    duration
    approvedDate
    campaignLocation
    carsToBeDeployed
    CAMPAIGNSTATUS
  }
}
`;
export const onUpdateCampaignPost = `subscription OnUpdateCampaignPost {
  onUpdateCampaignPost {
    id
    campaignName
    startDate
    duration
    approvedDate
    campaignLocation
    carsToBeDeployed
    CAMPAIGNSTATUS
  }
}
`;
export const onDeleteCampaignPost = `subscription OnDeleteCampaignPost {
  onDeleteCampaignPost {
    id
    campaignName
    startDate
    duration
    approvedDate
    campaignLocation
    carsToBeDeployed
    CAMPAIGNSTATUS
  }
}
`;
export const onCreateCampaignLocation = `subscription OnCreateCampaignLocation {
  onCreateCampaignLocation {
    id
    locationName
    state
    country
    postcodes
    mapImageUrl
    estReach
    population
    estNumberVehicles
    regDrivers
  }
}
`;
export const onUpdateCampaignLocation = `subscription OnUpdateCampaignLocation {
  onUpdateCampaignLocation {
    id
    locationName
    state
    country
    postcodes
    mapImageUrl
    estReach
    population
    estNumberVehicles
    regDrivers
  }
}
`;
export const onDeleteCampaignLocation = `subscription OnDeleteCampaignLocation {
  onDeleteCampaignLocation {
    id
    locationName
    state
    country
    postcodes
    mapImageUrl
    estReach
    population
    estNumberVehicles
    regDrivers
  }
}
`;
export const onCreateCampaignPostContent = `subscription OnCreateCampaignPostContent {
  onCreateCampaignPostContent {
    id
    CONTENTSTATUS
    contentStatusDeclineCause
    artworkFile
    artworkComment
    createdAt
  }
}
`;
export const onUpdateCampaignPostContent = `subscription OnUpdateCampaignPostContent {
  onUpdateCampaignPostContent {
    id
    CONTENTSTATUS
    contentStatusDeclineCause
    artworkFile
    artworkComment
    createdAt
  }
}
`;
export const onDeleteCampaignPostContent = `subscription OnDeleteCampaignPostContent {
  onDeleteCampaignPostContent {
    id
    CONTENTSTATUS
    contentStatusDeclineCause
    artworkFile
    artworkComment
    createdAt
  }
}
`;
