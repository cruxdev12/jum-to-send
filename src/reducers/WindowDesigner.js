/**
 * File Created for RYBW
 */
import {
  ON_SHOW_SAVE_DESIGN_LOADER,
  ON_HIDE_SAVE_DESIGN_LOADER,
  SHOW_NOTIFICATION,
  SHOW_ERROR_MSG,
  SHOW_SUCCESS_MSG,
  CAMPAIGNS_MODAL,
  SAVED_CAMPAIGNS_DATA
} from '../constants/ActionTypes';

const INIT_STATE = {
  loader: false,
  showNotification: false,
  showErrorMsg: '',
  showSuccessMsg: '',
  campaignsModal: false,
  savedCampaignsData: [],

  hasSavedCampaign: true
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case ON_SHOW_SAVE_DESIGN_LOADER: {
      console.log('in ON_SHOW_SAVE_DESIGN_LOADER');
      return {
        ...state,
        loader: true
      };
    }
    case ON_HIDE_SAVE_DESIGN_LOADER: {
      return {
        ...state,
        loader: false
      };
    }
    case SHOW_NOTIFICATION: {
      return {
        ...state,
        showNotification: action.payload
      };
    }
    case SHOW_ERROR_MSG: {
      return {
        ...state,
        showErrorMsg: action.payload
      };
    }
    case SHOW_SUCCESS_MSG: {
      return {
        ...state,
        showSuccessMsg: action.payload,
        hasSavedCampaign: true
      };
    }
    case CAMPAIGNS_MODAL: {
      return {
        ...state,
        campaignsModal: action.payload
      };
    }
    case SAVED_CAMPAIGNS_DATA: {
      return {
        ...state,
        savedCampaignsData: action.payload
      };
    }
    default:
      return state;

  }
};
