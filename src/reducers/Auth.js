/**
 * File Modified
 * Connected to AWS cognito
 */
import {
  HIDE_MESSAGE,
  INIT_URL,
  ON_HIDE_LOADER,
  ON_SHOW_LOADER,
  SHOW_MESSAGE,
  SIGNIN_USER_SUCCESS,
  SIGNOUT_USER_SUCCESS,
  SIGNUP_USER_SUCCESS,
  SHOW_AUTH_CONFIRMATION_CODE_MODAL,
  HIDE_AUTH_CONFIRMATION_CODE_MODAL,
  SHOW_FORGOT_PASSWORD_MODAL
} from '../constants/ActionTypes';

const INIT_STATE = {
  loader: false,
  alertMessage: '',
  showMessage: false,
  initURL: '',
  authUser: localStorage.getItem('clientId'),
  showConfirmCodeModal: false,
  showForgotPasswordModal: false
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {

    case SHOW_FORGOT_PASSWORD_MODAL: {
      return {
        ...state,
        showForgotPasswordModal: action.payload,
        loader: false
      };
    }

    case SIGNUP_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        authUser: action.payload // it receives the cognitoUserName
      };
    }
    case SIGNIN_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        authUser: action.payload // it receives the cognitoUserName
      };
    }

    case SIGNOUT_USER_SUCCESS: {
      return {
        ...state,
        authUser: null,
        initURL: '/app/dashboard',
        loader: false
      };
    }

    case SHOW_MESSAGE: {
      if (action.payload.message) {
        return {
          ...state,
          alertMessage: action.payload.message,
          showMessage: true,
          loader: false
        };
      }
      return {
        ...state,
        alertMessage: action.payload,
        showMessage: true,
        loader: false
      };
    }

    case HIDE_MESSAGE: {
      return {
        ...state,
        alertMessage: '',
        showMessage: false,
        loader: false
      };
    }

    case SHOW_AUTH_CONFIRMATION_CODE_MODAL: {
      return {
        ...state,
        showConfirmCodeModal: true,
        loader: false
      };
    }
    case HIDE_AUTH_CONFIRMATION_CODE_MODAL: {
      return {
        ...state,
        showConfirmCodeModal: false,
        loader: false
      };
    }

    case INIT_URL: {
      return {
        ...state,
        initURL: action.payload
      };
    }
    case ON_SHOW_LOADER: {
      return {
        ...state,
        loader: true
      };
    }
    case ON_HIDE_LOADER: {
      return {
        ...state,
        loader: false
      };
    }
    default:
      return state;
  }
};
