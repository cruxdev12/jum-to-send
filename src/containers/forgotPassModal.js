/**
 * File created for RYBW
 */
import React from 'react';
import {Modal, ModalHeader, ModalBody} from 'reactstrap';

const forgotPassModal = props => (
  <div>
    <Modal isOpen={props.showForgotPasswordModal}>
      <ModalHeader>
        <p>Header</p>
      </ModalHeader>

      <ModalBody>
        <p>Body</p>
      </ModalBody>

    </Modal>
  </div>
);

export default forgotPassModal;
