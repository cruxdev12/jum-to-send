/**
 * File Modified for RYBW
 * Added AWS
 */
import React from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import TextField from '@material-ui/core/TextField';
// import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import IntlMessages from '../util/IntlMessages';
import CircularProgress from '@material-ui/core/CircularProgress';
// import ForgotPassModal from './forgotPassModal';
import {
  hideMessage,
  showAuthLoader,
  userSignIn,
//   userFacebookSignIn,
//   userGithubSignIn,
//   userGoogleSignIn,
//   userTwitterSignIn
} from '../actions/Auth';

class SignIn extends React.Component {
  constructor() {
    super();
    this.state = {
      email: 'contact@cruxdev.com.au',
      password: '1234567',
      showForgotPasswordModal: false
    };
  }

  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }

  showforgotPassModal = () => {
    this.setState({
      showForgotPasswordModal: !this.state.showForgotPasswordModal
    });
  }

  render() {
    const {
      email,
      password
    } = this.state;
    const {showMessage, loader, alertMessage} = this.props;
    return (
      <div
        className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
        <div className="app-login-main-content">

          <div className="app-logo-content d-flex align-items-center justify-content-center">
            <Link className="logo-lg" to="/" title="Jambo">
              <img src="https://via.placeholder.com/177x65" alt="jambo" title="jambo" />
            </Link>
          </div>

          <div className="app-login-content">
            <div className="app-login-header mb-4">
              <h1><IntlMessages id="appModule.email" /></h1>
            </div>

            <div className="app-login-form">
              <form>
                <fieldset>
                  <TextField
                    label={<IntlMessages id="appModule.email" />}
                    fullWidth
                    onChange={event => this.setState({email: event.target.value})}
                    defaultValue={email}
                    margin="normal"
                    className="mt-1 my-sm-3"
                  />
                  <TextField
                    type="password"
                    label={<IntlMessages id="appModule.password" />}
                    fullWidth
                    onChange={event => this.setState({password: event.target.value})}
                    defaultValue={password}
                    margin="normal"
                    className="mt-1 my-sm-3"
                  />

                  <div className="mb-3 d-flex align-items-center justify-content-between">
                    <Button
                      onClick={() => {
                        this.props.showAuthLoader();
                        this.props.userSignIn({email, password});
                      }}
                      variant="contained"
                      color="primary">
                      <IntlMessages id="appModule.signIn" />
                    </Button>

                    <div className="d-flex flex-column">
                      <Link className="p-2" to="/signup">
                        <IntlMessages id="signIn.signUp" />
                      </Link>

                      <Link className="p-2" to="/forgotPassword">
                        <IntlMessages id="rybw.forgotPassword" />
                      </Link>
                    </div>
                  </div>

                </fieldset>
              </form>
            </div>
          </div>


          <Modal className="rybw-forgot-password-modal" isOpen={this.state.showForgotPasswordModal}>
            <ModalHeader>
              <IntlMessages id="rybw.resetPasswordModalHeader" />
            </ModalHeader>
            <ModalBody>
              <form>
                <fieldset>
                  <TextField
                    label={<IntlMessages id="appModule.email" />}
                    fullWidth
                    onChange={event => this.setState({email: event.target.value})}
                    defaultValue={email}
                    margin="normal"
                    className="mt-1 my-sm-3"
                  />
                  <TextField
                    type="password"
                    label={<IntlMessages id="appModule.password" />}
                    fullWidth
                    onChange={event => this.setState({password: event.target.value})}
                    defaultValue={password}
                    margin="normal"
                    className="mt-1 my-sm-3"
                  />
                </fieldset>
              </form>
            </ModalBody>

            <ModalFooter>
              <Button className="p-2 rybw-forgot-password-button" color="primary" variant="contained" onClick={() => {}}>
                <IntlMessages id="rybw.resetPasswordButton" />
              </Button>

              <Button className="p-2 rybw-forgot-password-button" color="secondary" variant="contained" onClick={this.showforgotPassModal}>
                <IntlMessages id="rybw.closeButton" />
              </Button>
            </ModalFooter>
          </Modal>


        </div>
        {
          loader
          && (
          <div className="loader-view">
            <CircularProgress />
          </div>
          )
        }
        {showMessage && NotificationManager.error(alertMessage)}
        <NotificationContainer />
      </div>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {loader, alertMessage, showMessage, authUser} = auth;
  return {loader, alertMessage, showMessage, authUser};
};

export default connect(mapStateToProps, {
  userSignIn,
  hideMessage,
  showAuthLoader,
//   userFacebookSignIn,
//   userGoogleSignIn,
//   userGithubSignIn,
//   userTwitterSignIn
})(SignIn);
