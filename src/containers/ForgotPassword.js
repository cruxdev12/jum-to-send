/**
 * File created for RYBW
 */
import React from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import TextField from '@material-ui/core/TextField';
// import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import IntlMessages from '../util/IntlMessages';
import CircularProgress from '@material-ui/core/CircularProgress';
// import ForgotPassModal from './forgotPassModal';
import {
  hideMessage,
  showAuthLoader,
  forgotPasswordAction,
  forgotPasswordSubmitAction
} from '../actions/Auth';

class ForgotPassword extends React.Component {
  constructor() {
    super();
    this.state = {
      email: 'demo@example.com',
      authCode: '',
      newPassword: ''
    };
  }

  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }
    // if (this.props.authUser !== null) {
    //   this.props.history.push('/');
    // }
  }

  render() {
    const {
      email,
      authCode,
      newPassword
    } = this.state;

    const {showMessage, loader, alertMessage, showForgotPasswordModal} = this.props;

    return (
      <div
        className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
        <div className="app-login-main-content">

          <div className="app-logo-content d-flex align-items-center justify-content-center">
            <Link className="logo-lg" to="/" title="Jambo">
              <img src="https://via.placeholder.com/177x65" alt="jambo" title="jambo" />
            </Link>
          </div>

          <div className="app-login-content">
            <div className="app-login-header mb-4">
              <h1><IntlMessages id="rybw.resetPasswordModalHeader" /></h1>
            </div>

            <div className="app-login-form">
              <form>
                <fieldset>
                  <TextField
                    label={<IntlMessages id="appModule.email" />}
                    fullWidth
                    onChange={event => this.setState({email: event.target.value})}
                    defaultValue={email}
                    margin="normal"
                    className="mt-1 my-sm-3"
                  />

                  <div className="mb-3 d-flex align-items-center justify-content-between">
                    <Button
                      onClick={() => {
                        this.props.showAuthLoader();
                        this.props.forgotPasswordAction({email});
                      }}
                      variant="contained"
                      color="primary">
                      <IntlMessages id="rybw.reset" />
                    </Button>

                    <div className="d-flex flex-column">
                      <Link className="p-2" to="/signup">
                        <IntlMessages id="signIn.signUp" />
                      </Link>

                      <Link className="p-2" to="/signin">
                        <IntlMessages id="signUp.alreadyMember" />
                      </Link>
                    </div>
                  </div>

                </fieldset>
              </form>
            </div>
          </div>


          <Modal className="rybw-forgot-password-modal" isOpen={showForgotPasswordModal}>
            <ModalHeader>
              <IntlMessages id="rybw.resetPasswordModalHeader" />
            </ModalHeader>
            <ModalBody>
              <p>Auth code was sent to your email:</p>
              <form>
                <fieldset>
                  {/* Pass auth code  and newPassword */}
                  <TextField
                    label={<IntlMessages id="rybw.authCode" />}
                    fullWidth
                    onChange={event => this.setState({authCode: event.target.value})}
                    defaultValue={authCode}
                    margin="normal"
                    className="mt-1 my-sm-3"
                  />

                  <TextField
                    label={<IntlMessages id="rybw.enterNewPassword" />}
                    fullWidth
                    onChange={event => this.setState({newPassword: event.target.value})}
                    defaultValue={newPassword}
                    margin="normal"
                    className="mt-1 my-sm-3"
                  />
                </fieldset>
              </form>
            </ModalBody>

            <ModalFooter>
              {/* pass Email and COde to SAGA */}
              <Button
                className="p-2 rybw-forgot-password-button"
                color="primary"
                variant="contained"
                onClick={() => {
                  this.props.showAuthLoader();
                  this.props.forgotPasswordSubmitAction({email, authCode, newPassword});
                }}
              >
                <IntlMessages id="rybw.reset" />
              </Button>

              <Button className="p-2 rybw-forgot-password-button" color="secondary" variant="contained" onClick={this.showforgotPassModal}>
                <IntlMessages id="rybw.closeButton" />
              </Button>
            </ModalFooter>
          </Modal>


        </div>
        {
          loader
          && (
          <div className="loader-view">
            <CircularProgress />
          </div>
          )
        }
        {showMessage && NotificationManager.error(alertMessage)}
        <NotificationContainer />
      </div>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {loader, alertMessage, showMessage, authUser, showForgotPasswordModal} = auth;
  return {loader, alertMessage, showMessage, authUser, showForgotPasswordModal};
};

export default connect(mapStateToProps, {
  forgotPasswordSubmitAction,
  forgotPasswordAction,
  hideMessage,
  showAuthLoader,
//   userFacebookSignIn,
//   userGoogleSignIn,
//   userGithubSignIn,
//   userTwitterSignIn
})(ForgotPassword);
