/**
 * File Modified for RYBW
 * Added AWS
 */
import React from 'react';
import { Row, Col } from 'reactstrap';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {NotificationContainer, NotificationManager} from 'react-notifications';

import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
// import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

import IntlMessages from '../util/IntlMessages';
import {
  hideMessage,
  showAuthLoader,
  userSignUp,
  enterConfirmCode
//   userFacebookSignIn, userGithubSignIn, userGoogleSignIn, userTwitterSignIn
} from '../actions/Auth';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const hearAboutUsChoices = [
  {
    value: 'google',
    label: 'Google',
  },
  {
    value: 'facebook',
    label: 'Facebook',
  },
  {
    value: 'linkedIn',
    label: 'LinkedIn',
  },
  {
    value: 'Flyer',
    label: 'Flyer',
  },
  {
    value: 'other',
    label: 'Other',
  },
];

class SignUp extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      companyName: '',
      companyAddress: '',
      companyPhoneNumber: '',
      firstName: '',
      lastName: '',
      jobTitle: '',
      directPhoneOrExt: '',
      hearAboutUs: '',
      //   agreedTcPp: false,

      authCode: '',
      showPassword: false,
    };
  }

  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 3000);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }

    // used on TC & PP
    handleClick = name => (event) => {
      this.setState({
        [name]: event.target.checked,
      });
    };

    handleClickShowPassword = () => {
      this.setState(state => ({ showPassword: !state.showPassword }));
    };

    handleChange = name => (event) => {
      this.setState({
        [name]: event.target.value,
      });
    };

    signUp = () => {
      const {
        email,
        password,
        companyName,
        companyAddress,
        companyPhoneNumber,
        firstName,
        lastName,
        jobTitle,
        directPhoneOrExt,
        hearAboutUs,
        // agreedTcPp,
      } = this.state;

      this.props.userSignUp({
        email,
        password,
        companyName,
        companyAddress,
        companyPhoneNumber,
        firstName,
        lastName,
        jobTitle,
        directPhoneOrExt,
        hearAboutUs,
        // agreedTcPp
      });
    }

    sendAuthCode = () => {
      const {authCode} = this.state;
      this.props.enterConfirmCode(authCode);
    }

    render() {
      //   console.log('value-directPhoneOrExt:', this.state.directPhoneOrExt);
    //   const { email, password, } = this.state;
      const {showMessage, loader, alertMessage, showConfirmCodeModal} = this.props;
      return (
        <div
          className="rybw-app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
          <div className="app-login-main-content">

            <div className="rybw-app-logo-content d-flex align-items-center justify-content-center">
              <Link className="logo-lg" to="/" title="Jambo">
                <img src="https://via.placeholder.com/177x65" alt="jambo" title="jambo" />
              </Link>
            </div>

            <div className="rybw-app-login-content">
              <div className="app-login-header ">
                <h1>Sign Up</h1>
              </div>

              <div className="mb-4">
                <h2><IntlMessages id="appModule.createAccount" /></h2>
              </div>

              <div className="app-login-form">
                <form method="post" action="/">
                  <Row>
                    <Col sm="12" md={{ size: 6, offset: 0 }}>
                      <TextField
                        required
                        fullWidth
                        margin="normal"
                        className="mt-0 mb-2"
                        label="Company Email"
                        onChange={this.handleChange('email')}
                      />

                      <FormControl required fullWidth className="mt-0 mb-2">
                        <InputLabel htmlFor="adornment-password">Password</InputLabel>
                        <Input
                          type={this.state.showPassword ? 'text' : 'password'}
                          value={this.state.password}
                          onChange={this.handleChange('password')}
                          endAdornment={(
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="Toggle password visibility"
                                onClick={this.handleClickShowPassword}
                              >
                                {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                            </InputAdornment>
)}
                        />
                      </FormControl>

                      <TextField
                        required
                        fullWidth
                        margin="normal"
                        className="mt-0 mb-2"
                        label="Company Name"
                        onChange={this.handleChange('companyName')}
                      />

                      <TextField
                        required
                        fullWidth
                        margin="normal"
                        className="mt-0 mb-2"
                        label="Company Address"
                        onChange={this.handleChange('companyAddress')}
                      />

                      <TextField
                        required
                        fullWidth
                        margin="normal"
                        className="mt-0 mb-2"
                        label="Company Phone Number"
                        onChange={this.handleChange('companyPhoneNumber')}
                      />
                    </Col>

                    <Col sm="12" md={{ size: 6, offset: 0 }}>
                      <TextField
                        required
                        fullWidth
                        margin="normal"
                        className="mt-0 mb-2"
                        label="First Name"
                        onChange={this.handleChange('firstName')}
                      />

                      <TextField
                        required
                        fullWidth
                        margin="normal"
                        className="mt-0 mb-2"
                        label="Last Name"
                        onChange={this.handleChange('lastName')}
                      />

                      <TextField
                        fullWidth
                        margin="normal"
                        className="mt-0 mb-2"
                        label="Job Title"
                        onChange={this.handleChange('jobTitle')}
                      />

                      <TextField
                        fullWidth
                        margin="normal"
                        className="mt-0 mb-2"
                        label="Phone Number or Extension"
                        onChange={this.handleChange('directPhoneOrExt')}
                      />

                      <TextField
                        required
                        className="mt-0 mb-2"
                        fullWidth
                        select
                        label="How did you hear about us?"
                        onChange={this.handleChange('hearAboutUs')}
                        value={this.state.hearAboutUs}
                      >
                        {hearAboutUsChoices.map(option => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Col>
                  </Row>

                  <div className="d-flex mt-3 mb-2 align-items-center">
                    {/* <Checkbox
                      required
                      className="align-items-center mb-2"
                      color="primary"
                      checked={this.state.agreedTcPp}
                      value="agreedTcPp"
                      onChange={this.handleClick('agreedTcPp')}
                    /> */}
                    {/* <h4>I agree to the Terms and Conditions and Privacy Policy.</h4> */}
                    <h4>By clicking "Register" I agree that I have read and accepted the Terms of Use.</h4>
                  </div>

                  {/* REGISTER BUTTON */}
                  <div className="mb-3 d-flex align-items-center justify-content-between">
                    <Button
                      variant="contained"
                      onClick={() => {
                        this.props.showAuthLoader();
                        this.signUp();
                      }}
                      color="primary">
                      <IntlMessages id="appModule.regsiter" />
                    </Button>
                    <Link to="/signin">
                      <IntlMessages id="signUp.alreadyMember" />
                    </Link>
                  </div>
                </form>
              </div>
            </div>
          </div>

          {
            loader
              && (
              <div className="loader-view">
                <CircularProgress />
              </div>
              )
          }
          {showMessage && NotificationManager.error(alertMessage)}
          <NotificationContainer />

          <Dialog
            open={showConfirmCodeModal}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">Enter Confirmation Code</DialogTitle>
            <DialogContent>
              <DialogContentText>
                A confirmation code has been sent to your Email.
              </DialogContentText>
              <DialogContentText color="secondary">
                Please check your junk mail too.
              </DialogContentText>
              <TextField
                fullWidth
                margin="normal"
                className="mt-0 mb-2"
                label="Enter Code"
                onChange={this.handleChange('authCode')}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="secondary">
              Cancel
              </Button>
              <Button
                color="primary"
                onClick={() => {
                  this.props.showAuthLoader();
                  this.sendAuthCode();
                //   this.props.enterConfirmCode(this.state.authCode);
                }}
              >
              Send
              </Button>

            </DialogActions>
          </Dialog>
        </div>
      );
    }
}

const mapStateToProps = ({auth}) => {
  const {loader, alertMessage, showMessage, authUser, showConfirmCodeModal} = auth;
  return {loader, alertMessage, showMessage, authUser, showConfirmCodeModal};
};
// send to actions
export default connect(mapStateToProps, {
  userSignUp,
  hideMessage,
  showAuthLoader,
  enterConfirmCode,
//   userFacebookSignIn,userGoogleSignIn,userGithubSignIn,userTwitterSignIn
})(SignUp);
