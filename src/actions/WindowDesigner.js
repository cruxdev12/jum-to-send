/**
 * File Created for RYBW
 */
import {
  SAVE_CAMPAIGN_POST,
  SAVE_CAMPAIGN_POST_IMG,
  LOAD_SAVED_CAMPAIGNS,
  CAMPAIGNS_MODAL,
  SAVED_CAMPAIGNS_DATA,
  ON_SHOW_SAVE_DESIGN_LOADER,
  ON_HIDE_SAVE_DESIGN_LOADER,
  SHOW_NOTIFICATION,
  SHOW_ERROR_MSG,
  SHOW_SUCCESS_MSG,
} from '../constants/ActionTypes';

export const saveCampaignPost = campData => ({
  type: SAVE_CAMPAIGN_POST,
  payload: campData
});

export const saveCampaignPostImg = payload => ({
  type: SAVE_CAMPAIGN_POST_IMG,
  payload
});

export const loadSavedCampaigns = () => ({
  type: LOAD_SAVED_CAMPAIGNS,

});

export const campaignsModalAction = cond => ({
  type: CAMPAIGNS_MODAL,
  payload: cond
});

export const savedCampaignsDataAction = data => ({
  type: SAVED_CAMPAIGNS_DATA,
  payload: data
});

export const showLoader = () => ({
  type: ON_SHOW_SAVE_DESIGN_LOADER
});

export const hideLoader = () => ({
  type: ON_HIDE_SAVE_DESIGN_LOADER,
});

export const showNotification = cond => ({
  type: SHOW_NOTIFICATION,
  payload: cond
});

export const showErrorMsg = msg => ({
  type: SHOW_ERROR_MSG,
  payload: msg
});

export const showSuccessMsg = msg => ({
  type: SHOW_SUCCESS_MSG,
  payload: msg
});
