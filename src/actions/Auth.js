/**
 * File Modified for RYBW
 * Connected to AWS cognito
 */
import {
  HIDE_MESSAGE,
  INIT_URL,
  ON_HIDE_LOADER,
  ON_SHOW_LOADER,
  SHOW_MESSAGE,
  SIGNIN_USER,
  SIGNIN_USER_SUCCESS,
  SIGNOUT_USER,
  SIGNOUT_USER_SUCCESS,
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SHOW_AUTH_CONFIRMATION_CODE_MODAL,
  HIDE_AUTH_CONFIRMATION_CODE_MODAL,
  ENTER_AUTH_CONFIRMATION_CODE_MODAL,
  FORGOT_PASSWORD,
  SHOW_FORGOT_PASSWORD_MODAL,
  FORGOT_PASSWORD_SUBMIT
} from '../constants/ActionTypes';

export const showForgotPasswordModal = cond => ({
  type: SHOW_FORGOT_PASSWORD_MODAL,
  payload: cond
});

export const forgotPasswordAction = email => ({
  type: FORGOT_PASSWORD,
  payload: email
});

export const forgotPasswordSubmitAction = data => ({
  type: FORGOT_PASSWORD_SUBMIT,
  payload: data
});

export const userSignUp = user => ({
  type: SIGNUP_USER,
  payload: user
});
export const enterConfirmCode = authCode => ({
  type: ENTER_AUTH_CONFIRMATION_CODE_MODAL,
  payload: authCode
});

export const userSignIn = user => ({
  type: SIGNIN_USER,
  payload: user
});
export const userSignOut = () => ({
  type: SIGNOUT_USER
});

export const userSignUpSuccess = authUser => ({
  type: SIGNUP_USER_SUCCESS,
  payload: authUser
});
export const userSignInSuccess = authUser => ({
  type: SIGNIN_USER_SUCCESS,
  payload: authUser
});
export const userSignOutSuccess = () => ({
  type: SIGNOUT_USER_SUCCESS,
});

export const showAuthMessage = message => ({
  type: SHOW_MESSAGE,
  payload: message
});
export const hideMessage = () => ({
  type: HIDE_MESSAGE,
});

export const showAuthLoader = () => ({
  type: ON_SHOW_LOADER,
});
export const hideAuthLoader = () => ({
  type: ON_HIDE_LOADER,
});

export const setInitUrl = url => ({
  type: INIT_URL,
  payload: url
});

export const showAuthConfirmCodeModal = () => ({
  type: SHOW_AUTH_CONFIRMATION_CODE_MODAL
});
export const hideAuthConfirmCodeModal = () => ({
  type: HIDE_AUTH_CONFIRMATION_CODE_MODAL
});
