import React, { Component, Fragment} from 'react';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import CustomTableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import Input from '@material-ui/core/Input';
// import InputLabel from '@material-ui/core/InputLabel';
// import FormHelperText from '@material-ui/core/FormHelperText';
// import IntlMessages from 'util/IntlMessages';

let id = 0;
function createData(days, percentage) {
  id += 1;
  return { id, days, percentage};
}

const rows = [
  createData('28 days', '0%'),
  createData('56 days', '20%'),
  createData('84 days', '30%'),
  createData('168 days', '40%'),
  createData('252 days', '50%'),
  createData('336 days', '60%'),
];


class Duration extends Component {
  render() {
    return (
      <Fragment>
        <div className="d-flex flex-row justify-content-around">
          <div className="d-flex flex-column justify-content-start">
            <br />
            <div className="p-3">
              <h1>Lets pick some dates!</h1>
              {/* <h1>campDate: {this.props.campDate}</h1>
              <h1>Dur: {this.props.campaignDuration}</h1> */}
            </div>
            <div className="p-3">
              {/* https://react.rocks/tag/DatePicker */}
              {/* https://codeburst.io/using-a-datepicker-in-react-js-b67e970195fd */}
              <h3>Choose a start date:</h3>
              <TextField
                //   id="date"
                required
                value={this.props.campDate}
                type="date"
                variant="outlined"
                onChange={this.props.handleChange('campaignStartDate')}
              />
            </div>
            <div className="p-3">
              <h3>Set duration:</h3>
              <FormControl className="rybwApp-formControl">
                {/* <InputLabel htmlFor="duration-helper">Set Duration</InputLabel> */}
                <NativeSelect
                  required
                  value={this.props.campaignDuration}
                  onChange={this.props.handleChange('campaignDuration')}
                  input={<Input name="duration" id="duration-helper" />}
                >
                  <option value="" />
                  <option value={28}>28 days (1 month)</option>
                  <option value={56}>56 days (2 months)</option>
                  <option value={84}>84 days (3 months)</option>
                  <option value={112}>112 days (4 months)</option>
                  <option value={140}>140 days (5 months)</option>
                  <option value={168}>168 days (6 months)</option>
                  <option value={196}>168 days (7 months)</option>
                  <option value={224}>168 days (8 months)</option>
                  <option value={252}>168 days (9 months)</option>
                  <option value={280}>168 days (10 months)</option>
                  <option value={308}>168 days (11 months)</option>
                  <option value={336}>336 days (12 months)</option>
                </NativeSelect>
                {/* <FormHelperText>Some important helper text</FormHelperText> */}
              </FormControl>
            </div>
            <br />
          </div>

          <div className="d-flex flex-column justify-content-start">
            <br />
            <div className="p-0">
              <h3>Discount based duration:</h3>
              <Paper className="rybwApp-table-root">
                <Table className="rybwApp-table">
                  <TableHead>
                    <TableRow>
                      <CustomTableCell><h4>Duration</h4></CustomTableCell>
                      <CustomTableCell>
                        <h4>Discount</h4>
                      </CustomTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map(row => (
                      <TableRow key={row.id} className="rybwApp-row">
                        <CustomTableCell component="th" scope="row">{row.days}</CustomTableCell>
                        <CustomTableCell className="rybwApp-tableCell">{row.percentage}</CustomTableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Paper>
            </div>
            <br />
          </div>

        </div>
      </Fragment>
    );
  }
}

export default Duration;
