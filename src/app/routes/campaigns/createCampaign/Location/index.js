import React, { Component, Fragment} from 'react';
// import ContainerHeader from 'components/ContainerHeader/index';
// import IntlMessages from 'util/IntlMessages';
import { InputGroup, InputGroupAddon, Input, Form, FormGroup } from 'reactstrap';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class Location extends Component {
  render() {
    let areaCard;
    if (this.props.campArea === 'Perth') {
      areaCard = (
        <Card>
          <CardHeader
            title="Perth Area"
            subheader="Population: 1.9M |  vehicles: 580K  | reach: 1.6M"
        />
          <CardMedia
            className="rybwApp-media"
          //   image={require('./img/perth-region.png')}
          //   image="https://s3-ap-southeast-2.amazonaws.com/rybw-shared-files-201901/campaignLocation/perth-region.png"
            image="https://rentyourbackwindow.com.au/wp-content/uploads/2019/01/perth-region.png"
            alt="Something went wrong"
            title="Perth Area"
        />
        </Card>
      );
    } if (this.props.campArea === 'Mandurah') {
      areaCard = (
        <Card>
          <CardHeader
            title="Mandurah Area"
            subheader="Population: 100k |  vehicles: 50K  | reach: 148k"
        />
          <CardMedia
            className="rybwApp-media"
          //   image="https://s3-ap-southeast-2.amazonaws.com/rybw-shared-files-201901/campaignLocation/mandurah-region.png"
            image="https://rentyourbackwindow.com.au/wp-content/uploads/2019/01/mandurah-region.png"
            title="Mandurah Area"
        />
        </Card>
      );
    } if (this.props.campArea === 'Perth and Mandurah') {
      areaCard = (
        <Card>
          <CardHeader
            title="Perth and Mandurah Area"
            subheader="Population: 1.9M |  vehicles: 580K  | reach: 1.75M"
        />
          <CardMedia
            className="rybwApp-media"
          //   image="https://s3-ap-southeast-2.amazonaws.com/rybw-shared-files-201901/campaignLocation/perth-mandurah-region.png"
            image="https://rentyourbackwindow.com.au/wp-content/uploads/2019/01/perth-mandurah-region.png"
            title="Perth and Mandurah Area"
        />
        </Card>
      );
    }

    return (
      <Fragment>
        <div className="d-flex flex-row justify-content-around">
          <div className="d-flex flex-column justify-content-start">
            <br />
            <div className="p-3">
              <h1>Set up the locations for your campaign:</h1>
              <h3>1) Choose an area to start with:</h3>
              <FormGroup>
                <Input
                  type="select"
                  name="selectLocation"
                  value={this.props.campArea}
                  onChange={this.props.handleChange('campaignArea')}
                >
                  <option value="" />
                  <option value="Perth">Perth - WA</option>
                  <option value="Mandurah">Mandurah - WA</option>
                  <option value="Perth and Mandurah">Perth and Mandurah - WA</option>
                </Input>
              </FormGroup>
            </div>

            <div className="p-3">
              {areaCard}
            </div>
          </div>

          <div className="d-flex flex-column justify-content-start">
            <br />
            <br />
            <br />
            <div className="p-3">
              <h3>2) How many vehicles will be deployed in this area?</h3>
              <Form>
                <FormGroup>
                  <Input
                    type="number"
                    name="vehiclesNumbers"
                    placeholder="Number of vehicles"
                    value={this.props.campCars}
                    onChange={this.props.handleChange('campaignCars')}
                  />
                </FormGroup>
              </Form>
            </div>

            {/* <div className="p-3">
              <h3>Estimate number of daily impressions: 5000</h3>
            </div> */}

            <div className="p-3">
              <h3>3) How much will you pay the drivers per week?</h3>
              <Paper className="d-flex">
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>KM Brackets</TableCell>
                      <TableCell>Weekly Payment*</TableCell>
                    </TableRow>
                  </TableHead>

                  <TableBody>
                    <TableRow>
                      <TableCell>0 - 50 kms</TableCell>
                      <TableCell>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">$</InputGroupAddon>
                          <Input
                            placeholder="Amount"
                            type="number"
                            step="5"
                            value={this.props.bracket1}
                            onChange={this.props.handleChange('bracket1')}
                          />
                          <InputGroupAddon addonType="append">.00</InputGroupAddon>
                        </InputGroup>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>51 - 100 kms</TableCell>
                      <TableCell>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">$</InputGroupAddon>
                          <Input
                            placeholder="Amount"
                            type="number"
                            step="5"
                            value={this.props.bracket2}
                            onChange={this.props.handleChange('bracket2')}
                          />
                          <InputGroupAddon addonType="append">.00</InputGroupAddon>
                        </InputGroup>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>101 - 150 kms</TableCell>
                      <TableCell>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">$</InputGroupAddon>
                          <Input
                            placeholder="Amount"
                            type="number"
                            step="5"
                            value={this.props.bracket3}
                            onChange={this.props.handleChange('bracket3')}
                          />
                          <InputGroupAddon addonType="append">.00</InputGroupAddon>
                        </InputGroup>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>over 151 kms</TableCell>
                      <TableCell>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">$</InputGroupAddon>
                          <Input
                            placeholder="Amount"
                            type="number"
                            step="5"
                            value={this.props.bracket4}
                            onChange={this.props.handleChange('bracket4')}
                          />
                          <InputGroupAddon addonType="append">.00</InputGroupAddon>
                        </InputGroup>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </Paper>
              <p className="rybwApp-margin-text">*The suggested amount are shown on the table.</p>
            </div>
          </div>

        </div>
      </Fragment>
    );
  }
}

export default Location;
