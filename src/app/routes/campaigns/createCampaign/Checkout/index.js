import React from 'react';
import { API } from 'aws-amplify';
import StripeCheckout from 'react-stripe-checkout';
import Button from '@material-ui/core/Button';


const stripeConfig = {
  currency: 'AUD',
  publishableKey: 'pk_test_FnO00D26CpET3sglQRGn4hxy'
};

const email = localStorage.getItem('email');

const Checkout = ({ name, description, amount }) => {

  console.log('email: ', email);

  const onToken = (amount, description) => async (token) => {
    try {
      const result = await API.post('stripeapi', '/stripe', {
        body: {
          token,
          charge: {
            currency: stripeConfig.currency,
            amount,
            description
          }
        }
      });
      console.log('StripeCheckout result:', result);
      //   history.push('/app/campaigns/PaymentReceived');

    } catch (err) {
      console.log('StripeCheckout ERROR:', err);
    }
  };

  return (
    <StripeCheckout
      name={name}
      description={description}
      amount={amount}
      email={email}
      token={onToken(amount, description, email)}
      stripeKey={stripeConfig.publishableKey}
      image="https://rentyourbackwindow.com.au/wp-content/uploads/2019/01/rentyourbackwindow_logo_1x.png"
      currency={stripeConfig.currency}
      locale="auto"
      allowRememberMe={false}
    >
      <Button
        variant="contained"
        color="primary"
      >
Submit

      </Button>
    </StripeCheckout>
  );
};

export default Checkout;
