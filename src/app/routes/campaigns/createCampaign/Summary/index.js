import React, { Component, Fragment} from 'react';
// import ContainerHeader from 'components/ContainerHeader/index';
// import IntlMessages from 'util/IntlMessages';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
// import CardContent from '@material-ui/core/CardContent';
// import CardActions from '@material-ui/core/CardActions';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import TextField from '@material-ui/core/TextField';
import { Button } from 'reactstrap';

class Summary extends Component {

  componentDidMount = () => {
    this.props.getArtworkPreview();
    this.props.calculate();
  }

  render() {
    let areaCard;
    if (this.props.campArea === 'Perth') {
      areaCard = (
<Card>
        <CardHeader
          title="Perth Area"
          subheader="Population: 1.9M |  vehicles: 580K  | reach: 1.6M"
        />
        <CardMedia
          className="rybwApp-media-summary"
          image="https://rentyourbackwindow.com.au/wp-content/uploads/2019/01/perth-region.png"
          alt="Something went wrong"
          title="Perth Area"
        />
      </Card>
);
    } if (this.props.campArea === 'Mandurah') {
      areaCard = (
<Card>
        <CardHeader
          title="Mandurah Area"
          subheader="Population: 100k |  vehicles: 50K  | reach: 148k"
        />
        <CardMedia
          className="rybwApp-media-summary"
          //   image="https://s3-ap-southeast-2.amazonaws.com/rybw-shared-files-201901/campaignLocation/mandurah-region.png"
          image="https://rentyourbackwindow.com.au/wp-content/uploads/2019/01/mandurah-region.png"
          title="Mandurah Area"
        />
      </Card>
);
    } if (this.props.campArea === 'Perth and Mandurah') {
      areaCard = (
<Card>
        <CardHeader
          title="Perth and Mandurah Area"
          subheader="Population: 1.9M |  vehicles: 580K  | reach: 1.75M"
        />
        <CardMedia
          className="rybwApp-media-summary"
          //   image="https://s3-ap-southeast-2.amazonaws.com/rybw-shared-files-201901/campaignLocation/perth-mandurah-region.png"
          image="https://rentyourbackwindow.com.au/wp-content/uploads/2019/01/perth-mandurah-region.png"
          title="Perth and Mandurah Area"
        />
      </Card>
);
    }

    return (
      <Fragment>
        <div className="d-flex flex-row justify-content-start">
          <div className="p-4">
            <h1>CAMPAIGN SUMMARY:</h1>
          </div>
        </div>

        <div className="d-flex flex-row justify-content-around">

          <Paper className="d-flex flex-fill flex-column justify-content-start rybwApp-summary-paper">
            <h2>Let's go over the details and make sure everything is just right.</h2>

            <div className="rybwApp-border">
              <div className="d-flex flex-row justify-content-around">
                <div className="p-2 align-items-center">
                  <h3 className="rybwApp-center-text rybwApp-margin-text">Campaign ID:</h3>
                  <h2 className="rybwApp-center-text rybwApp-bold-text rybwApp-margin-text">{this.props.campaignID}</h2>
                </div>
                <div className="p-2 align-items-center ">
                  <h3 className="rybwApp-margin-text">Campaign Name:</h3>
                  <h2 className="rybwApp-center-text rybwApp-bold-text rybwApp-margin-text">{this.props.campName}</h2>
                </div>
              </div>
              {/* <div className="d-flex flex-column">
                <p className="rybwApp-center-text">Let's go over the details and make sure everything is just right.</p>
              </div> */}
            </div>

            <div className="rybwApp-border">
              <div className="d-flex flex-row justify-content-around">
                <div className="p-2 align-items-center">
                  <h3 className="rybwApp-center-text rybwApp-margin-text">Start Date:</h3>
                  <h2 className="rybwApp-center-text rybwApp-bold-text rybwApp-margin-text">{this.props.campDate}</h2>
                </div>
                <div className="p-2 align-items-center">
                  <h3 className="rybwApp-margin-text">Duration:</h3>
                  <h2 className="rybwApp-center-text rybwApp-margin-text">
                    <span className="rybwApp-bold-text">
{this.props.campDuration}
{' '}
 </span>
days
</h2>
                </div>
                <div className="p-2 align-items-center">
                  <h3 className="rybwApp-margin-text">Number of vehicles:</h3>
                  <h2 className="rybwApp-center-text rybwApp-bold-text rybwApp-margin-text">{this.props.campCars}</h2>
                </div>
              </div>
              <div className="d-flex flex-column">
                <p className="rybwApp-center-text rybwApp-no-margin-text">The longer you run your ads, the more you save!</p>
              </div>
            </div>

            <div className="rybwApp-border">
              <div className="d-flex flex-row justify-content-around">

                <div className="d-flex flex-column justify-content-start">
                  <div className="p-2">
                    <h3 className="rybwApp-margin-text">Amount you will pay drivers per week:</h3>
                  </div>
                  <div className="p-2">
                    <Paper>
                      <Table>
                        <TableHead>
                          <TableRow>
                            <TableCell><h4 className="rybwApp-no-margin-text">KM Brackets</h4></TableCell>
                            <TableCell><h4 className="rybwApp-no-margin-text">Weekly Payment</h4></TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          <TableRow>
                            <TableCell>
                              <h4 className="rybwApp-center-text rybwApp-no-margin-text">0 - 50km</h4>
                            </TableCell>
                            <TableCell>
                              <h3 className="rybwApp-center-text rybwApp-bold-text rybwApp-no-margin-text">
                                {new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(this.props.bracket1)}
                              </h3>
                            </TableCell>
                          </TableRow>

                          <TableRow>
                            <TableCell>
                              <h4 className="rybwApp-center-text rybwApp-no-margin-text">51 - 100km</h4>
                            </TableCell>
                            <TableCell>
                              <h3 className="rybwApp-center-text rybwApp-bold-text rybwApp-no-margin-text">
                                {new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(this.props.bracket2)}
                              </h3>
                            </TableCell>
                          </TableRow>

                          <TableRow>
                            <TableCell>
                              <h4 className="rybwApp-center-text rybwApp-no-margin-text">101 - 150km</h4>
                            </TableCell>
                            <TableCell>
                              <h3 className="rybwApp-center-text rybwApp-bold-text rybwApp-no-margin-text">
                                {new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(this.props.bracket3)}
                              </h3>
                            </TableCell>
                          </TableRow>

                          <TableRow>
                            <TableCell>
                              <h4 className="rybwApp-center-text rybwApp-no-margin-text">over 151km</h4>
                            </TableCell>
                            <TableCell>
                              <h3 className="rybwApp-center-text rybwApp-bold-text rybwApp-no-margin-text">
                                {new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(this.props.bracket4)}
                              </h3>
                            </TableCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                    </Paper>
                  </div>
                </div>

                <div className="d-flex flex-column justify-content-center">
                  <div className="p-2">
                    <h2 className="rybwApp-center-text rybwApp-margin-text">Total Deposit:</h2>
                    <h1 className="rybwApp-center-text rybwApp-bold-text rybwApp-margin-text">
                      {new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(this.props.driverPayment)}
                    </h1>
                  </div>
                  <div className="p-2">
                    <p className="rybwApp-center-text rybwApp-bold-text rybwApp-margin-text">Some of this amount will be refunded based on the KMs that haven't been driven.</p>
                  </div>
                </div>

              </div>
            </div>

            <div className="rybwApp-border">
              <div className="d-flex justify-content-center">
                <div className="p-3">
                  <p className="rybwApp-center-text rybwApp-no-margin-text">
                  Your card will be charged 
{' '}
<span className="rybwApp-bold-text">{new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(this.props.campSetupCost)}</span>
{' '}
to cover the campaign setup costs (
{new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(this.props.costPerVehicleDiscounted)}
{' '}
times the
{' '}
{this.props.campCars}
{' '}
vehicles you requested, over campaign duration)
                  plus
{' '}
<span className="rybwApp-bold-text">{new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(this.props.driverPayment)}</span>
{' '}
for the maximun number of kilometres each vehicle can drive while the campaign is running.
                  Making a total deposit of
{' '}
<span className="rybwApp-bold-text">{new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(this.props.totalDeposit)}</span>
{' '}
for the
{' '}
<span className="rybwApp-bold-text">{this.props.campDuration} </span>
days.
                  The amount will be refunded if you do not choose to activate the campaign.
</p>
                </div>
              </div>

              <div className="d-flex flex-row justify-content-around">
                <div className="d-flex flex-column">
                  <div className="p-2">
                    <TextField
                      id="outlined-number"
                      label="Discount code"
                      value={this.props.discountCode}
                      onChange={this.props.handleChange('discountCode')}
                    />
                  </div>
                  <div className="p-2 align-self-center">
                    <Button
                      color="success"
                      onClick={() => { this.props.calculateDiscountCode(); }}
                    >
APPLY

                    </Button>
                  </div>
                </div>

                <div className="d-flex flex-column">
                  <div className="p-2">
                    <h1 className="rybwApp-large-text">
{new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(this.props.totalDeposit)}
*
</h1>
                  </div>
                  <div className="p-2">
                    <p className="rybwApp-center-text rybwApp-no-margin-text">* excluding GST</p>
                    <p className="rybwApp-center-text rybwApp-no-margin-text">Deposit amount due upon submission</p>
                  </div>
                </div>
              </div>

            </div>
          </Paper>


          <div className="d-flex flex-column justify-content-start">
            <div className="p-2">
              <h3>Target Location:</h3>
              {areaCard}
            </div>
            <br />
            <div className="p-2">
              <h3>Back Window Design:</h3>
              <Card>
                <CardMedia
                  className="rybwApp-artworkPreview-summary"
                  image={this.props.artworkPreviewUrl}
                  src="img"
                  title="Artwork Preview"
                />
              </Card>
              <p className="rybwApp-margin-text">We will contact you before printing it.</p>
            </div>
          </div>

        </div>


      </Fragment>
    );
  }
}

export default Summary;
