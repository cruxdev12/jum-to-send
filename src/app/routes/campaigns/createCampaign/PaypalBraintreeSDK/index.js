import React from 'react';
import { API } from 'aws-amplify';
import Button from '@material-ui/core/Button';
// import {history} from '../../../../../../../store';

// const Paypal = ({ name, description, amount }) => {
const Paypal = () => {
  const amount = 100;
  const currency = 'AUD';
  const orderId = 'AU-123';


  const callPaypal = async () => {
    try {
      console.log('ON callPaypal');
      const nonce = await API.get('paypalapi', '/paypal');
      console.log('Paypal nonce: ', nonce);
      const transaction = await API.post('paypalapi', '/paypal', {
        body: {
          amount,
          currency,
          paymentMethodNonce: nonce,
          orderId,
          descriptor: {
            name: 'Descriptor displayed in customer CC statements'
          },
          enableShippingAddress: false,
        }
      });
      console.log('Paypal transaction: ', transaction);

    } catch (err) {
      console.log('Paypal ERROR:', err);
    }
  };

  return (
    <Button
      variant="contained"
      color="primary"
      //   onClick={() => { console.log('ON callPaypal'); }}
      onClick={() => { callPaypal(); }}
    >
Paypal

    </Button>
  );
};

export default Paypal;
