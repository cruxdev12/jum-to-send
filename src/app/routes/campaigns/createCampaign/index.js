/**
 * File Created for RYBW
 */
import React, { Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Storage} from 'aws-amplify';
// import IntlMessages from 'util/IntlMessages';
// import ContainerHeader from 'components/ContainerHeader/index';
// import ContainerHeader from '../../../../../components/ContainerHeader/index';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

// import {history} from '../../../../../store';
import CampName from './Name';
import Duration from './Duration';
import Location from './Location';
import ArtWork from './Artwork';
import Summary from './Summary';
import Checkout from './Checkout';
import {
  saveCampaignPost,
  saveCampaignPostImg,
  loadSavedCampaigns,
  campaignsModalAction
} from '../../../../actions/WindowDesigner';

function getSteps() {
  return ['Name', 'Duration', 'Location', 'Artwork', 'Summary'];
}

// create Campaign ID
function createUUID() {
  let dt = new Date().getTime();
  const my = `${new Date().getFullYear()}${new Date().getMonth() + 1}`;
  const uuid = `campAu-${my}${'-yxxxxx'.replace(/[xy]/g, (c) => {
    // eslint-disable-next-line
    const r = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    // eslint-disable-next-line
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  })}`;
  return uuid;
}

const initialState = {
  activeStep: 0,
  campaignID: '',
  campaignName: 'test',
  campaignStartDate: '2019-01-01',
  campaignDuration: '56',
  campaignArea: 'Perth and Mandurah',
  campaignCars: 5,
  artworkComment: '',
  bracket1: 0,
  bracket2: 15,
  bracket3: 30,
  bracket4: 50,

  driverPayment: '',
  totalDeposit: '',
  costPerVehicle: 200,
  costPerVehicleDiscounted: '',
  campSetupCost: '',
  discountCode: '',
  hasAppliedDiscountCode: false,
  artworkPreviewUrl: '',
  artworkJSONFile: ''
};

class HorizontalLinearStepper extends Component {
//   static propTypes = {
//     classes: PropTypes.objectOf(PropTypes.object()),
//   };
    state = initialState;

    getStepContent(step) {
      switch (step) {
        case 0:
        //   return (<CampName
        //     campName={this.state.campaignName}
        //     handleChange={this.handleChange.bind(this)}
        //     loader={this.props.loader}
        //     saveCampaignPostImgHandler={this.saveCampaignPostImgHandler.bind(this)}
        //     saveCampaignPost={this.props.saveCampaignPost}
        //   />);

          return (
            <ArtWork
              campaignID={this.state.campaignID}
              campName={this.state.campaignName}
              campDate={this.state.campaignStartDate}
              campaignDuration={this.state.campaignDuration}
              campArea={this.state.campaignArea}
              campCars={this.state.campaignCars}
              artworkComment={this.state.artworkComment}
              artworkJSONFile={this.state.artworkJSONFile}

              hasSavedCampaign={this.props.hasSavedCampaign}
              handleChange={this.handleChange.bind(this)}
              saveArtworkJSONFile={this.saveArtworkJSONFile.bind(this)}

              saveCampaignPostImg={this.props.saveCampaignPostImg}
              saveCampaignPost={this.props.saveCampaignPost}

              showNotification={this.props.showNotification}
              showSuccessMsg={this.props.showSuccessMsg}
              showErrorMsg={this.props.showErrorMsg}
              loader={this.props.loader}
              campaignsModal={this.props.campaignsModal}
              loadSavedCampaigns={this.props.loadSavedCampaigns}
              campaignsModalAction={this.props.campaignsModalAction}
              savedCampaignsData={this.props.savedCampaignsData}
          />
          );

        case 1:
          return (
            <Duration
              campDate={this.state.campaignStartDate}
              campaignDuration={this.state.campaignDuration}
              handleChange={this.handleChange.bind(this)}
          />
          );
        case 2:
          return (
            <Location
              campArea={this.state.campaignArea}
              campCars={this.state.campaignCars}
              handleChange={this.handleChange.bind(this)}
              bracket1={this.state.bracket1}
              bracket2={this.state.bracket2}
              bracket3={this.state.bracket3}
              bracket4={this.state.bracket4}
          />
          );
        case 3: // ARTWORK
          return (
            <CampName
              campName={this.state.campaignName}
              handleChange={this.handleChange.bind(this)}
              loader={this.props.loader}
              saveCampaignPostImgHandler={this.saveCampaignPostImgHandler.bind(this)}
              saveCampaignPost={this.props.saveCampaignPost}
          />
          );
        case 4:
          return (
            <Summary
              campaignID={this.state.campaignID}
              campName={this.state.campaignName}
              campDate={this.state.campaignStartDate}
              campDuration={this.state.campaignDuration}
              campArea={this.state.campaignArea}
              campCars={this.state.campaignCars}

              bracket1={this.state.bracket1}
              bracket2={this.state.bracket2}
              bracket3={this.state.bracket3}
              bracket4={this.state.bracket4}

              calculate={this.calculate.bind(this)}
              driverPayment={this.state.driverPayment}
              totalDeposit={this.state.totalDeposit}
              campSetupCost={this.state.campSetupCost}
              discountCode={this.state.discountCode}
              calculateDiscountCode={this.calculateDiscountCode.bind(this)}
              handleChange={this.handleChange.bind(this)}
              getArtworkPreview={this.getArtworkPreview.bind(this)}
              artworkPreviewUrl={this.state.artworkPreviewUrl}
              costPerVehicleDiscounted={this.state.costPerVehicleDiscounted}
          />
          );
        default:
          return 'Unknown step';
      }
    }

    getArtworkPreview = () => {
      console.log('in getArtworkPreview');
      return Promise.resolve(
        Storage.get(`campaignsContent/${this.state.campaignID}/artwork.jpeg`, { level: 'private' })
          .then((result) => {
            // console.log('then Storage.get!', result);
            this.setState({artworkPreviewUrl: result});
          })
      );
    };

    saveCampaignPostImgHandler = () => {
      console.log('saveCampaignPostImgHandler');
      this.props.saveCampaignPost();
    }

    calculate = () => {
      console.log('Start calculate');
      let months;
      let monthDiscount;
      if (this.state.campaignDuration === '28') {
        months = 1;
        monthDiscount = 1;
      } else if (this.state.campaignDuration === '56') {
        months = 2;
        monthDiscount = 0.8;
      } else if (this.state.campaignDuration === '84') {
        months = 3;
        monthDiscount = 0.7;
      } else if (this.state.campaignDuration === '112') {
        months = 4;
        monthDiscount = 0.7;
      } else if (this.state.campaignDuration === '140') {
        months = 5;
        monthDiscount = 0.7;
      } else if (this.state.campaignDuration === '168') {
        months = 6;
        monthDiscount = 0.6;
      } else if (this.state.campaignDuration === '196') {
        months = 7;
        monthDiscount = 0.6;
      } else if (this.state.campaignDuration === '224') {
        months = 8;
        monthDiscount = 0.6;
      } else if (this.state.campaignDuration === '252') {
        months = 9;
        monthDiscount = 0.5;
      } else if (this.state.campaignDuration === '280') {
        months = 10;
        monthDiscount = 0.5;
      } else if (this.state.campaignDuration === '308') {
        months = 11;
        monthDiscount = 0.5;
      } else if (this.state.campaignDuration === '336') {
        months = 12;
        monthDiscount = 0.4;
      } else {
        console.log('Months ERROR');
      }
      console.log('months: ', months);
      console.log('monthDiscount: ', monthDiscount);

      const costPerVehicleDiscounted = this.state.costPerVehicle * monthDiscount;
      this.setState({costPerVehicleDiscounted});
      console.log('costPerVehicleDiscounted: ', costPerVehicleDiscounted);

      const driverPayment = (this.state.campaignCars * (this.state.bracket4 * 4)) * months;
      this.setState({driverPayment});

      const totalDeposit = ((this.state.campaignCars * costPerVehicleDiscounted) * months) + driverPayment;
      this.setState({totalDeposit});

      const campSetupCost = (this.state.campaignCars * costPerVehicleDiscounted) * months;
      this.setState({campSetupCost});
    }

    calculateDiscountCode = () => {
      console.log('Start calculateDiscount');
      if (this.state.discountCode === 'discount10') { // valid code
        this.setState({hasAppliedDiscountCode: true});
        if (!this.state.hasAppliedDiscountCode) { // have already used code
          const totalDeposit = this.state.totalDeposit * 0.90;
          this.setState({totalDeposit});
        } else {
          console.log('Discount has been applied');
        }
      } else {
        console.log('INvalid discount');
      }
    }

    saveArtworkJSONFile = (artworkJSONFile) => {
      console.log('In saveArtworkJSONFile:');
      this.setState({artworkJSONFile});
    }

    handleChange = name => (event) => {
      this.setState({
        [name]: event.target.value,
      });
    };

    handleNext = () => {
      const { activeStep } = this.state;
      this.setState({
        activeStep: activeStep + 1
      });
    };

    handleFirstStep = () => {
      const { activeStep } = this.state;
      this.setState({
        activeStep: activeStep + 1
      });

      if (!this.state.campaignID) {
        const campId = createUUID();
        this.setState({
          campaignID: campId
        });
      }
    }

    handleBack = () => {
      this.setState(state => ({
        activeStep: state.activeStep - 1,
      }));
    };

    handleReset = () => {
      this.setState({
        activeStep: 0,
      });
    };

    render() {
      const steps = getSteps();
      const { activeStep } = this.state;

      let backButton;
      if (activeStep === 0) {
        backButton = (<Button onClick={() => { console.log('back'); }}>Back</Button>);
      } else {
        backButton = (<Button onClick={this.handleBack}>Back</Button>);
      } // NEED TO CREATE THE BACK BUTTON FOR ARTWORK - USER NEEDST O SAVE BEFORE MOVING

      let nextButton;
      if (activeStep === 0) { // Name
        nextButton = (
          <Button
            disabled={this.state.campaignName.length < 4}
          // disabled={this.props.loader}
            variant="contained"
            color="primary"
            onClick={this.handleFirstStep}
        >
Next

          </Button>
        );

      } if (activeStep === 1) { // Duration
        nextButton = (
          <Button
            disabled={!this.state.campaignStartDate || !this.state.campaignDuration}
            variant="contained"
            color="primary"
            onClick={this.handleNext}
        >
Next

          </Button>
        );
      } if (activeStep === 2) { // Location
        nextButton = (
          <Button
            disabled={!this.state.campaignArea || !this.state.campaignCars}
            variant="contained"
            color="primary"
            onClick={this.handleNext}
        >
Next

          </Button>
        );
      } if (activeStep === 3) { // Artwork
        nextButton = (
          <Button
            disabled={this.props.loader || !this.props.hasSavedCampaign}
            variant="contained"
            color="primary"
            onClick={this.handleNext}
        >
Next

          </Button>
        );
      } if (activeStep === 4) { // Summary
        const description = String(
          `$${this.state.costPerVehicleDiscounted} X ${this.state.campaignCars} cars over ${this.state.campaignDuration} days, plus driver payment of $${this.state.driverPayment}`
        );
        const amount = Math.round((((this.state.totalDeposit * 1.1) * 100) * 1e2) / 1e2); // calculate GST
        nextButton = (
          <Checkout
            name="RentYourBackWindow" // the pop-in header title
            description={description}
            amount={amount}
          />
        );
      }

      return (
        <div className="app-wrapper">
          <div>
            <Stepper activeStep={activeStep}>
              {steps.map((label, index) => {
                const props = {};
                const labelProps = {};
                /* if (this.isStepOptional(index)) {
                labelProps.optional = <Typography variant="caption">Optional</Typography>;
              } */
                return (
                  <Step key={label} {...props}>
                    <StepLabel {...labelProps}>{label}</StepLabel>
                  </Step>
                );
              })}
            </Stepper>
            <div>
              {activeStep === steps.length ? (
                <div>
                  <Typography>
                All steps completed - you&apos;re finished
                  </Typography>
                  <Button onClick={this.handleReset}>
                Reset
                  </Button>
                </div>
              ) : (
                <div>
                  <Fragment>{this.getStepContent(activeStep)}</Fragment>
                  <div>
                    {backButton}
                    {nextButton}
                  </div>
                </div>
              )}
            </div>
          </div>

        </div>
      );
    }
}

// HorizontalLinearStepper.propTypes = {
//   classes: PropTypes.objectOf(PropTypes.object()), // PropTypes.object,
// };

const mapStateToProps = ({wd}) => {
//   console.log('mapStateToProps wd: ', {wd});
  const { loader, showNotification, showSuccessMsg, showErrorMsg, campaignsModal, savedCampaignsData, hasSavedCampaign } = wd;
  return { loader, showNotification, showSuccessMsg, showErrorMsg, campaignsModal, savedCampaignsData, hasSavedCampaign };
};

export default connect(mapStateToProps, {saveCampaignPost, saveCampaignPostImg, loadSavedCampaigns, campaignsModalAction})(HorizontalLinearStepper);
