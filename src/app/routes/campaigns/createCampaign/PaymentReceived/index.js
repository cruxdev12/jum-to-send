/**
 * TODO:
 * Handle undefined props - if occured - must unlikely
 */
import React, { Component } from 'react';
// import ContainerHeader from 'components/ContainerHeader/index';
import ContainerHeader from '../../../../../../components/ContainerHeader/index';

// import Card from '@material-ui/core/Card';
// import CardHeader from '@material-ui/core/CardHeader';
// import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import {history} from '../../../../../../store';

class PaymentReceived extends Component {
  render() {
    // const amount = 3900;
    // const description = '$160 X 5 cars over 56 days, driver payment of $2000';
    // const receipt_email = 'rfcctba@hotmail.com';
    // const receipt_url = 'https://pay.stripe.com/receipts/acct_1DNCXpJc9zc5rF0F/ch_1E0nWMJc9zc5rF0FM7NDR9mB/rcpt_ETl26IziS92bpuNk6Wim6N2a2Qm7xlh';

    const { amount, description, receipt_email, receipt_url} = this.props.location.state.data;
    if (amount === undefined || description === undefined || receipt_email === undefined || receipt_url === undefined) {
      return (
        <div className="app-wrapper">
          <ContainerHeader match={this.props.match} title="Payment Received" />
          <div className="d-flex flex-row justify-content-around">
            <div className="d-flex flex-column">
              <div className="p-3">
                <h2>Sorry, something went wrong!</h2>
              </div>
              <div className="p-3">
                <Button
                  color="primary"
                  variant="contained"
                  onClick={() => { history.push('/app/campaigns'); }}
                >
                Contact Support
                </Button>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="app-wrapper">
        <ContainerHeader match={this.props.match} title="Payment Received" />
        <div className="d-flex flex-row justify-content-around">
          <div className="d-flex flex-column justify-content-start">
            <div className="p-3">
              <h2>
                A Payment of
                <span className="rybwApp-bold-text">{new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'aud'}).format(amount / 100)}</span>
                was received sucessfull
              </h2>
              <br />
              <h3>
                <span className="rybwApp-bold-text">Description: </span>
                {description}
              </h3>
              <br />
              <h3>
                <span className="rybwApp-bold-text">Receipt was send to: </span>
                {receipt_email}
              </h3>
              <br />
              <h3>
                <span className="rybwApp-bold-text">Receipt can be seen at: </span>
                <a href={receipt_url} target="_blank" rel="noopener noreferrer">Link</a>
              </h3>
            </div>
            <div className="p-3">
              <Button
                color="primary"
                variant="contained"
                onClick={() => { history.push('/app/campaigns'); }}
              >
Back to Campaigns

              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PaymentReceived;
