import React, { Component, Fragment} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
// import ContainerHeader from 'components/ContainerHeader/index';
// import IntlMessages from 'util/IntlMessages';
// import {history} from '../../../../../../store';
// import {connect} from 'react-redux';
// import CircularProgress from '@material-ui/core/CircularProgress';
// import { showLoader } from '../../../../../../../actions/WindowDesigner';


class Name extends Component {
  render() {
    return (
      <Fragment>
        <div className="d-flex flex-column justify-content-start">
          <br />
          <div className="p-2">
            <TextField
              required
              label="Enter Campaign Name"
              margin="normal"
              variant="outlined"
              value={this.props.campName}
              onChange={this.props.handleChange('campaignName')}
            />
          </div>

          <div className="p-2">
            <Button
              color="primary"
              //   className="rybwApp-redButton"
              variant="contained"
              onClick={this.props.saveCampaignPostImgHandler}
            >
saveCampaignPostImgHandler

            </Button>
          </div>

        </div>
        <br />

      </Fragment>

    );
  }
}

export default Name;
