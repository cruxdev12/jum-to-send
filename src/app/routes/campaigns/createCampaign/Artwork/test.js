/**
* File created for rybw
*/
import $ from 'jquery';
import React, { Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Modal, ModalHeader, ModalBody, ModalFooter, Button, Table} from 'reactstrap';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import ContainerHeader from 'components/ContainerHeader/index';
import Card from '@material-ui/core/Card';
import CircularProgress from '@material-ui/core/CircularProgress';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

// import IntlMessages from 'util/IntlMessages';
// import {Helmet} from 'react-helmet';

class Artwork extends React.Component {


  componentDidMount() {
    $(document).ready(function () {
      const $fpd = $('#backWindow-designer');
      const pluginOpts = {
        stageWidth: 1200,
        stageHeight: 650,
        editorMode: false,
        smartGuides: true,
        fonts: [
          { name: 'Acme', url: 'google' },
          { name: 'Arial' },
          { name: 'Helvetica' },
          { name: 'Indie Flower', url: 'google' },
          { name: 'Lato', url: 'google' },
          { name: 'Lobster', url: 'google' },
          { name: 'Montserrat', url: 'google' },
          { name: 'Open Sans', url: 'google' },
          { name: 'Pacifico', url: 'google' },
          { name: 'Permanent Marker', url: 'google' },
          { name: 'Roboto', url: 'google' },
          { name: 'Times New Roman' },
          { name: 'Ubuntu', url: 'google' },
        ],
        customTextParameters: {
          zChangeable: true,
          colors: true,
          removable: true,
          resizable: true,
          draggable: true,
          rotatable: true,
          autoCenter: true,
          curvable: true,
          boundingBox: 'Guidelines'
        },
        customImageParameters: {
          zChangeable: true,
          draggable: true,
          removable: true,
          resizable: true,
          rotatable: true,
          colors: '#000',
          autoCenter: true,
          advancedEditing: false,
          boundingBox: 'Window Background'
        },
        actions: {
          right: ['magnify-glass', 'zoom', 'reset-product', 'ruler', 'snap'],
          bottom: ['undo', 'redo'],
          left: ['manage-layers', 'download', 'print']
        },
        mainBarModules: ['images', 'text', 'manage-layers']
      };

      const yourDesigner = new FancyProductDesigner($fpd, pluginOpts);

      // create an preview image
      $('#preview-button').click(() => {
        if (this.props.loader) {
          return false;
        }
        const image = yourDesigner.createImage();
        return false;
      });

      $('#save-button').click(() => {
        console.log('#save-button');
        if (this.props.loader) {
          return false;
        }
        // save design file which can be re-open on Back Window Designer
        const artworkJSONFile = JSON.stringify(yourDesigner.getProduct());
        this.props.saveArtworkJSONFile(artworkJSONFile);

        yourDesigner.getProductDataURL((dataURL) => {
          //   console.log('dataURL:', dataURL);
          // save SVG for printing
          const svgData = yourDesigner.getViewsSVG();

          const campData = {
            dataURL, // jpeg image
            svgData, // svg file
            artworkFile: artworkJSONFile, // file that can be re-open on Back Window Designer
            id: this.props.campaignID,
            campName: this.props.campName,
            campDate: this.props.campDate,
            campDuration: this.props.campaignDuration,
            campArea: this.props.campArea,
            campCars: this.props.campCars,
            artworkComment: this.props.artworkComment,
          };

          if (!this.props.hasSavedCampaign) {
            this.props.saveCampaignPost(campData);
          } else {
            console.log('UPDATE CAMPAING ID:', this.props.campaignID);
          }
        });
        return false;
      });

      $('#load-design2').click(() => {
        console.log('in load-design2');
        // const productJson = localStorage.getItem('productJson');
        // yourDesigner.loadProduct(JSON.parse(productJson));
        const productJson = this.props.artworkJSONFile;
        if (productJson) {
          yourDesigner.loadProduct(JSON.parse(productJson));
        }
        return false;
      });


      function loadDesign(artworkJSONFile) {
        console.log('in load-design');
        // console.log('artworkJSONFile: ', artworkJSONFile);
        // yourDesigner.loadProduct(artworkJSONFile);
        // // yourDesigner.loadProduct(JSON.parse(artworkJSONFile));

        // yourDesigner.loadProduct(productJson);
        // yourDesigner.loadProduct(JSON.parse(productJson));
        // return false;
      }
      //   const productJson = localStorage.getItem('productJson');
      //   if (productJson) {
      //     console.log('Trigger load-design');
      //     yourDesigner.loadProduct(JSON.parse(productJson));
      //     // loadDesign();
      //   }

      //   if (this.props.artworkJSONFile) {
      //     console.log('Trigger load-design');
      //     const artworkJSONFile = this.props.artworkJSONFile;
      //     // yourDesigner.loadProduct(artworkJSONFile);

      //     // loadDesign();
      //     yourDesigner.on('ready', () => { yourDesigner.loadProduct(JSON.parse(artworkJSONFile)); });
      //   }

      // Open modal to Load past design
      $('#load-product-db').click(() => {
        console.log('in load product db');
        if (this.props.loader) {
          return false;
        }
        this.props.loadSavedCampaigns(); // Load and open Modal
        // const productJson = localStorage.getItem('productJson');
        // yourDesigner.loadProduct(JSON.parse(productJson));
        return false;
      });

      // load design
      $('#load-design').click(() => {
        console.log('in load-design');
        //   const data = $(this).attr('data');
        //   const data = $(this).getAttribute('data');
        const data = $(this).data('data');
        yourDesigner.loadProduct(JSON.parse(data));
        //   this.loadDesignHandler();
        return false;
      });

    //   // function created to test on HOW to call a JQuery function from Handler
    //   function loadFromHandler(params) {
    //     console.log('in loadFromHandler');
    //     return false;
    //   }
    //   loadFromHandler();
    });
  }

  //   As soon as the component unmounts the designer is removed with all events inside.
  componentWillUnmount() {
    $('[class^="fpd-element-toolbar"]').remove();
  }

  // // function created to test on HOW to call a JQuery function from Handler
  //   testHandler = () => {
  //     console.log('in testHandler');
  //     const fpd = $('#backWindow-designer').data('instance');
  //     fpd.loadFromHandler();
  //   }


  render() {
    let buttonStyle;
    if (!this.props.loader) {
      buttonStyle = 'fpd-btn';
    } else {
      buttonStyle = 'fpd-btn-disable';
    }

    let notificationfMSG;
    if (this.props.showNotification && this.props.showSuccessMsg) {
      notificationfMSG = NotificationManager.success(this.props.showSuccessMsg);
    }
    if (this.props.showNotification && this.props.showErrorMsg) {
      notificationfMSG = NotificationManager.error(this.props.showErrorMsg);
    }
    if (!this.props.showNotification) {
      notificationfMSG = '';
    }

    // NOT BEING USED - LOADING NOT WORKING YET
    let savedCampDataModal;
    if (this.props.savedCampaignsData.length === 0 || this.props.savedCampaignsData.data.listCampaignPostContents.items.length === 0) {
      savedCampDataModal = (<h3>There are not back window designs on file</h3>);
    } else {
      const columns = [
        {
          Header: 'ID',
          accessor: 'id'
        },
        {
          Header: 'Created Date',
          accessor: 'createdAt'
        },
        {
          id: 'button',
          Header: 'Load Design',
          accessor: 'artworkFile',
          Cell: ({value}) => (
            <button id="load-design" artworkfile={value} >LOAD</button>
          )
          // Cell: ({value}) => (<button onClick={() => { this.loadDesignHandler(value); }} >LOAD</button>)
        }
      ];
      savedCampDataModal = (
        <ReactTable
          data={this.props.savedCampaignsData.data.listCampaignPostContents.items}
          columns={columns}
          defaultPageSize={5}
          className="-striped -highlight"
        />
      );
    }

    return (
      <Fragment>
        <div className="d-flex justify-content-center">
          <div id="backWindow-designer-container">
            <div className="d-flex flex-row justify-content-start">
              <div className="d-flex w-50 flex-column justify-content-start">
                <div className="p-2">
                  <h1>Back Window Designer:</h1>
                  <h3>Create the design for the 3 types of windows.</h3>
                </div>
              </div>

              <div className="d-flex w-50 flex-column justify-content-start">
                <div className="p-1">
                  <h2>Need extra help?</h2>
                </div>
                <div className="p-1 d-flex justify-content-around">
                  <Button className="rybwApp-redButton">Video Instruction</Button>
                  <Button className="rybwApp-redButton" >Let us design for you</Button>
                </div>
              </div>
            </div>
            <br />

            <div id="backWindow-designer" className="fpd-container fpd-shadow-2 fpd-topbar fpd-tabs fpd-tabs-side fpd-top-actions-centered fpd-bottom-actions-centered fpd-views-inside-left">

              <div className="fpd-product" title="Small Vehicle" data-thumbnail="productDesigner/images/backWindow/vehicle-small-preview.png">
                <img src="productDesigner/images/backWindow/vehicle-small-setup-base.png" title="Window Background" data-parameters='{"left": 600, "top": 329, "colorLinkGroup": "Window Background", "colors": "#ffffff"}' alt="Something went wrong. Refresh the page!" />
                <img src="productDesigner/images/backWindow/vehicle-small-setup-guidelines.png" title="Guidelines" data-parameters='{"left": 600, "top": 329, "zChangeable": true}' alt="Something went wrong. Refresh the page!" />
                <img src="productDesigner/images/backWindow/wiper.png" title="Wiper" data-parameters='{"left": 500, "top": 450, "zChangeable": true}' alt="Something went wrong. Refresh the page!" />

                <span title="Small Window Design" data-parameters='{"left": 600, "top": 110, "textSize": 25, "colors": "#000000", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>Small Window Design</span>
                <span title="External Bleed" data-parameters='{"left": 210, "top": 130, "zChangeable": true, "colors": "#FF0000", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>External Bleed</span>
                <span title="Window Trim Line" data-parameters='{"boundingBox": "Window Background", "left": 600, "top": 165, "colors": "#00AEEF", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>Window Trim Line</span>
                <span title="Safe Area" data-parameters='{"boundingBox": "Window Background", "left": 600, "top": 320,"colors": "#00A651", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>Safe Area</span>

                <div className="fpd-product" title="Medium Vehicle" data-thumbnail="productDesigner/images/backWindow/vehicle-medium-preview.png">
                  <img src="productDesigner/images/backWindow/vehicle-medium-setup-base.png" title="Window Background" data-parameters='{"left": 600, "top": 329, "zChangeable": true, "colorLinkGroup": "Window Background", "colors": "#ffffff", "zChangeable": false}' alt="Something went wrong. Refresh the page!" />
                  <img src="productDesigner/images/backWindow/vehicle-medium-setup-guidelines.png" title="Guidelines" data-parameters='{"left": 600, "top": 329, "zChangeable": true}' alt="Something went wrong. Refresh the page!" />
                  <img src="productDesigner/images/backWindow/wiper.png" title="Wiper" data-parameters='{"left": 490, "top": 490, "zChangeable": true}' alt="Something went wrong. Refresh the page!" />

                  <span title="Medium Window Design" data-parameters='{"left": 600, "top": 70, "textSize": 25, "colors": "#000000", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>Medium Window Design</span>
                  <span title="External Bleed" data-parameters='{"left": 260, "top": 75, "colors": "#FF0000", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>External Bleed</span>
                  <span title="Window Trim Line" data-parameters='{"boundingBox": "Window Background", "left": 600, "top": 120, "colors": "#00AEEF", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>Window Trim Line</span>
                  <span title="Safe Area" data-parameters='{"boundingBox": "Window Background", "left": 600, "top": 320, "colors": "#00A651", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>Safe Area</span>
                </div>

                <div className="fpd-product" title="Large Vehicle" data-thumbnail="productDesigner/images/backWindow/vehicle-large-preview.png">
                  <img src="productDesigner/images/backWindow/vehicle-large-setup-base.png" title="Window Background" data-parameters='{"left": 600, "top": 329, "zChangeable": true, "colorLinkGroup": "Window Background", "colors": "#ffffff", "zChangeable": false}' alt="Something went wrong. Refresh the page!" />
                  <img src="productDesigner/images/backWindow/vehicle-large-setup-guidelines.png" title="Guidelines" data-parameters='{"left": 600, "top": 329, "zChangeable": true}' alt="Something went wrong. Refresh the page!" />

                  <span title="Large Window Design" data-parameters='{"left": 600, "top": 110, "textSize": 25, "colors": "#000000", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>Large Window Design</span>
                  <span title="External Bleed" data-parameters='{"left": 210, "top": 130, "colors": "#FF0000", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>External Bleed</span>
                  <span title="Window Trim Line" data-parameters='{"boundingBox": "Window Background", "left": 600, "top": 165, "colors": "#00AEEF", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>Window Trim Line</span>
                  <span title="Safe Area" data-parameters='{"boundingBox": "Window Background", "left": 600, "top": 320, "colors": "#00A651", "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true}'>Safe Area</span>
                </div>
              </div>

            </div>
            <br /><br />

            <div className="d-flex flex-row justify-content-around">
              <div className="d-flex w-50 flex-column justify-content-start">
                <div className="p-2">
                  <h2>Enter any relevant comments about your design!</h2>
                  <Card className="rybwApp-textarea-card">
                    <textarea
                      // add handler to save comments
                      value={this.props.artworkComment}
                      onChange={this.props.handleChange('artworkComment')}
                      className="rybwApp-textarea"
                      placeholder="Such as fonts that aren't available."
                    />
                  </Card>
                </div>
              </div>

              <div className="d-flex w-60 flex-column justify-content-start">
                <div className="p-2">
                  <h1>Preview | Save </h1>
                </div>
                <div className="p-2 d-flex justify-content-between api-buttons fpd-container">
                  <a href="#" id="preview-button" className={buttonStyle}>Preview Designs</a>
                  <a href="#" id="save-button" className={buttonStyle}>Save Designs</a>
                  {/* <a href="#" id="load-product-db" className={buttonStyle}>Load Design</a> */}
                  <a href="#" id="load-design2" className={buttonStyle}>Load Design</a>


                  {/* <button
                    className={buttonStyle}
                    onClick={() => this.testHandler()}
                  >TEST handler</button> */}
                </div>
                {
                  this.props.loader &&
                    <div className="loader-backWindow-designer">
                      <CircularProgress />
                    </div>
                }
                { notificationfMSG }
                <NotificationContainer />
              </div>
            </div>

          </div>
        </div>

        { // Modal for loading past designs - LOADING NOT WORKING YET
          <Modal className="rybw-forgot-password-modal" isOpen={this.props.campaignsModal}>
            <ModalHeader>
            Designs on File
            </ModalHeader>
            <ModalBody>
              {savedCampDataModal}
            </ModalBody>
            <ModalFooter>
              <Button
                color="danger"
                onClick={() => this.props.campaignsModalAction(false)}
              >CLOSE</Button>
            </ModalFooter>
          </Modal>
        }

      </Fragment>
    );
  }
}

export default Artwork;
