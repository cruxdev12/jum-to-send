import React from 'react';
import {Helmet} from 'react-helmet';
import Plugin from './Plugin';

class ArtWork extends React.Component {
  render() {
    return (
      <div className="application">
        <Helmet>
          <meta charSet="utf-8" />
          <title>My Title</title>
          <script type="text/babel" src="./plugin/js/FancyProductDesigner.js" />
          <script type="text/babel" src="./plugin/js/plugin.js" />
        </Helmet>
        <Plugin
          campaignID={this.props.campaignID}
          campName={this.props.campaignName}
          campDate={this.props.campaignStartDate}
          campaignDuration={this.props.campaignDuration}
          campArea={this.props.campaignArea}
          campCars={this.props.campaignCars}
          artworkComment={this.props.artworkComment}
          artworkJSONFile={this.props.artworkJSONFile}

          hasSavedCampaign={this.props.hasSavedCampaign}
          handleChange={this.props.handleChange}
          saveArtworkJSONFile={this.props.saveArtworkJSONFile}

          saveCampaignPostImg={this.props.saveCampaignPostImg}
          saveCampaignPost={this.props.saveCampaignPost}

          showNotification={this.props.showNotification}
          showSuccessMsg={this.props.showSuccessMsg}
          showErrorMsg={this.props.showErrorMsg}
          loader={this.props.loader}
          campaignsModal={this.props.campaignsModal}
          loadSavedCampaigns={this.props.loadSavedCampaigns}
          campaignsModalAction={this.props.campaignsModalAction}
          savedCampaignsData={this.props.savedCampaignsData}
        />
      </div>
    );
  }
}

export default ArtWork;
