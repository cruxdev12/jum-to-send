import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';

const Dashboard = ({createCampaign}) => (
  <Fragment>
    <div className="d-flex flex-column justify-content-between">

      <h1>Campaign Dashboard</h1>
      <div className="p-2 bd-highlight">
        <h3>No campaigns to be shown.</h3>
        <h3>Create a campaign by pressing the button below.</h3>
      </div>

      <div className="p-2 bd-highlight">
        <Button
              //   color="primary"
          className="rybwApp-redButton"
          variant="contained"
          onClick={createCampaign}
            >
            Create Design
        </Button>
      </div>

    </div>
  </Fragment>
);


export default Dashboard;
