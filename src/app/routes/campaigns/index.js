import React, { Component, Fragment } from 'react';
import ContainerHeader from '../../../components/ContainerHeader/index';
import Dashboard from './dashboard';
import CreateCampaign from './createCampaign/index';

class Campaign extends Component {
  state = {
    create: true
  }

  createCampaign = () => {
    console.log('createCampaign');
    this.setState(prevState => ({
      create: !prevState.create,
    }));
  };

  render() {
    const { match } = this.props;
    const {create} = this.state;

    let content;
    if (create) {
      content = (
        <Fragment>
          <CreateCampaign />
        </Fragment>
      );
    } else {
      content = (
        <Fragment>
          <Dashboard
            createCampaign={this.createCampaign}
          />
        </Fragment>
      );
    }

    return (
      <div>
        <div className="app-wrapper">
          <ContainerHeader match={match} title="Campaign" />
          <div className="app-container">
            <div className="app-main-content app-wrapper">
              <div className="row">
                {content}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Campaign;
