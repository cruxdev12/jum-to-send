/**
 * File Modified for RYBW
 * Connected to AWS cognito
 */
import {all, call, fork, put, takeEvery} from 'redux-saga/effects';
import { Auth, API, Storage, graphqlOperation } from 'aws-amplify';
import { SIGNIN_USER, SIGNOUT_USER, SIGNUP_USER, ENTER_AUTH_CONFIRMATION_CODE_MODAL, FORGOT_PASSWORD, FORGOT_PASSWORD_SUBMIT} from '../constants/ActionTypes';
import {showAuthMessage, userSignInSuccess, userSignOutSuccess, showAuthConfirmCodeModal, hideAuthConfirmCodeModal, showForgotPasswordModal} from '../actions/Auth';
import {getClient} from '../graphql/queries';
import {updateClient} from '../graphql/mutations';

import {history} from '../store';

// Workers Saga - signUp confirmationCode Start
const authConfirmCodeModalRequest = async (username, code) => Auth.confirmSignUp(username, code, {
  forceAliasCreation: true // Optional. Force user confirmation irrespective of existing alias. By default set to True.
}).then(data => data)
  .catch(err => err);

function* authConfirmCodeModal(authCode) {
  console.log('IN authConfirmCodeModal');
  const code = authCode.payload;
  const username = localStorage.getItem('username');
  console.log('authConfirmCodeModal USERNAME: ', username);
  try { // enter code
    const confirmUser = yield call(authConfirmCodeModalRequest, username, code);
    // console.log('confirmUser is:', confirmUser);
    if ((confirmUser === 'Username cannot be empty') || (confirmUser === 'Code cannot be empty')) {
      yield put(showAuthMessage(confirmUser));
    } else if (confirmUser.message) { // SHOW ERROR MESSAGE
      yield put(showAuthMessage(confirmUser.message));
    } else {
    //   const clientId = localStorage.getItem('clientId');
      yield put(hideAuthConfirmCodeModal()); // HIDE CONFIRMATION MODAL
      localStorage.clear();
      history.push('/'); //   direct to sigIn
    }
  } catch (err) {
    const error = 'something went TOTALLY wrong on authConfirmCodeModal';
    yield put(showAuthMessage(error));
  }
}
// Workers Saga - signUp START
const createUserWithEmailPasswordRequest = async ({payload}) => {
  console.log('in createUserWithEmailPasswordRequest:');
  const {email, password, companyName, companyAddress, companyPhoneNumber, firstName, lastName, hearAboutUs} = payload;
  let { jobTitle, directPhoneOrExt } = payload;

  if (!jobTitle) {
    jobTitle = 'not provided';
  }
  if (!directPhoneOrExt) {
    directPhoneOrExt = 'not provided';
  }

  let phone;
  const firstTwoDigits = companyPhoneNumber.substring(0, 2);
  if (firstTwoDigits === '+61') {
    phone = companyPhoneNumber;
  } else {
    phone = `+61${companyPhoneNumber}`;
  }

  return Auth.signUp({
    username: email,
    password,
    attributes: {
      email,
      'custom:companyName': companyName,
      address: companyAddress,
      phone_number: phone,
      given_name: firstName,
      family_name: lastName,
      'custom:jobTitle': jobTitle,
      'custom:directPhoneOrExt': directPhoneOrExt,
      'custom:heardAboutUs': hearAboutUs
    }
  })
    .then(data => data)
    .catch(err => err);
};

function checkDetailsRequest({payload}) {
  console.log('in checkDetailsRequest');
  if (payload.companyName && payload.companyAddress && payload.hearAboutUs && payload.firstName && payload.lastName) {
    return true;
  }
  return false;
}

function* createUserWithEmailPassword({payload}) {
  console.log('in createUserWithEmailPassword');
  const checkDetails = yield call(checkDetailsRequest, {payload});
  if (!checkDetails) {
    yield put(showAuthMessage('Atrributes marked with * must be filled'));
  } else {
    console.log('username: ', payload.email);
    localStorage.setItem('username', payload.email); // used on authConfirmCodeModal later on
    try {
      const signUpUser = yield call(createUserWithEmailPasswordRequest, {payload});
      console.log('signUpUser: ', signUpUser);
      localStorage.setItem('id', signUpUser.userSub);
      if (signUpUser === 'Username cannot be empty' || signUpUser === 'Password cannot be empty') { // SHOW ERROR
        yield put(showAuthMessage(signUpUser));
      } else if (signUpUser.message) { // SHOW ERROR MESSAGE
        yield put(showAuthMessage(signUpUser.message));
      } else {
        yield put(showAuthConfirmCodeModal()); // SHOW CONFIRMATION MODAL
      }
    } catch (err) {
      const error = 'something went TOTALLY wrong on createUser';
      yield put(showAuthMessage(error));
    }
  }
}
// Workers Saga - signUp END

// Workers Saga - signIn START
async function setClientDetails() { // set Details from dynamo
  const id = localStorage.getItem('id');
  //   console.log('in setClientDetails ID', id);
  try {
    const user = await API.graphql(graphqlOperation(
      getClient, {id}
    ));
    Object.keys(user.data.getClient).forEach(
      key => localStorage.setItem(key, user.data.getClient[key])
    );
    // Set Federated Identity IF NULL
    const hasFedId = localStorage.getItem('federatedIdentity');
    if (hasFedId === 'null') {
      const fedId = {
        id,
        federatedIdentity: localStorage.getItem('fedId')
      };
      API.graphql(graphqlOperation(updateClient, {input: fedId}));
    }
    // get LOGO URL
    const hasUploadLogo = localStorage.getItem('hasUploadLogo');
    if (hasUploadLogo === 'true') {
      await Storage.get('Logo.jpg', { level: 'private' })
        .then((result) => {
          // https://github.com/aws-amplify/amplify-js/issues/2004
          // const url = 'https://s3-ap-southeast-2.amazonaws.com/rybwclientwebapp-userfiles-mobilehub-1823515120/private/ap-southeast-2%3Ab5a1dda1-563f-47dd-86a0-fde839860846/Logo.jpg';
          // console.log('setClientDetails Storage.get!', result);
          localStorage.setItem('companyLogoUrl', result);
        });
    }
  } catch (error) {
    console.log('setClientDetails error: ', error);
  }
}

//   console.log('in signInUserWithEmailPasswordRequest');
const signInUserWithEmailPasswordRequest = async (username, password) => Auth.signIn(username, password)
  .then((user) => {
    console.log('Auth.signIn: ', user);
    localStorage.setItem('id', user.username);
    localStorage.setItem('fedId', user.storage['aws.cognito.identity-id.ap-southeast-2:485f9757-6a5e-4392-9916-2b38b2d6fd85']);
    console.log('fedId: ', localStorage.getItem('fedId'));
    return user;
  })
  .catch((err) => {
    console.log('Auth.signIn ERR: ', err);
    return err;
  });
function* signInUserWithEmailPassword({payload}) {
//   console.log('in signInUserWithEmailPassword');
  const {email, password} = payload;
  try {
    const signInUser = yield call(signInUserWithEmailPasswordRequest, email, password);
    if (signInUser.message) {
      yield put(showAuthMessage(signInUser.message));
    } else {
      yield call(setClientDetails);
      yield put(userSignInSuccess(signInUser.username));
    }
  } catch (err) {
    const error = 'something went TOTALLY wrong on signin';
    yield put(showAuthMessage(error));
  }
}

// Workers Saga - signOut
const signOutRequest = async () => {
  console.log('in signOutRequest');
  Auth.signOut()
    .then(user => user)
    .catch(error => error);
};
function* signOut() {
  console.log('in signOut');
  try {
    const signOutUser = yield call(signOutRequest);
    if (signOutUser === undefined) {
      localStorage.clear();
      yield put(userSignOutSuccess(signOutUser));
    } else {
      yield put(showAuthMessage(signOutUser.message));
    }
  } catch (err) {
    const error = 'something went TOTALLY wrong signOut';
    yield put(showAuthMessage(error));
  }
}

// Workers Saga - forgotPassword
const forgotPasswordRequest = async (email) => {
  const username = email.email;
  //   console.log('in forgotPasswordRequest username is:', username);
  return Auth.forgotPassword(username)
    .then((user) => {
      console.log('forgotPasswordRequest USER: ', user);
      return user;
    })
    .catch((err) => {
      console.log('forgotPasswordRequest ERROR: ', err);
      return err;
    });
};
function* forgotPassword(email) {
//   console.log('in forgotPassword');
  try {
    const user = yield call(forgotPasswordRequest, email.payload);
    // console.log('forgotPassword USER: ', user);
    if (user.message) { // ERROR
    //   console.log('IF FALSE');
      yield put(showAuthMessage(user.message));
    } else { // SUCESS
      yield put(showForgotPasswordModal(true));
    }
  } catch (err) {
    const error = 'something went TOTALLY wrong forgotPassword';
    yield put(showAuthMessage(error));
  }
}

// Workers Saga - forgotPasswordSubmit
const forgotPasswordSubmitRequest = async (details) => {
  console.log('in forgotPasswordSubmitRequest');
  console.log('in forgotPasswordSubmit details ARE:', details);
  const username = details.email;
  const code = details.authCode;
  const new_password = details.newPassword;

  return Auth.forgotPasswordSubmit(username, code, new_password)
    .then((user) => {
      console.log('forgotPasswordSubmitRequest USER: ', user);
      return user;
    })
    .catch((err) => {
      console.log('forgotPasswordSubmitRequest ERROR: ', err);
      return err;
    });
};
function* forgotPasswordSubmit(details) {
  console.log('in forgotPasswordSubmit');
  try {
    const user = yield call(forgotPasswordSubmitRequest, details.payload);
    console.log('forgotPasswordSubmitRequest USER: ', user);
    if (user.message) { // ERROR
      console.log('IF FALSE - forgotPasswordSubmitRequest');
      yield put(showAuthMessage(user.message));
    } else { // SUCESS
      console.log('SUCCESS- forgotPasswordSubmitRequest');
      yield put(showForgotPasswordModal(false));
      localStorage.clear();
      history.push('/');
    }
  } catch (err) {
    const error = 'something went TOTALLY wrong forgotPasswordSubmitRequest';
    yield put(showAuthMessage(error));
  }
}


// Workers Saga - get details
// const getUserDetails = async () => {
//   console.log('in getUserDetails');
//   return Auth.currentAuthenticatedUser()
//     .then((user) => {
//       console.log('user details are:', user);
//       return user;
//     })
//     .catch((err) => {
//       console.log('Auth.currentAuthenticatedUser ERROR:', err);
//       return user;
//     });
// };

// Watchers Saga
export function* signInUser() {
  yield takeEvery(SIGNIN_USER, signInUserWithEmailPassword);
}

export function* createUserAccount() {
  yield takeEvery(SIGNUP_USER, createUserWithEmailPassword);
}

export function* enterConfCode() {
  yield takeEvery(ENTER_AUTH_CONFIRMATION_CODE_MODAL, authConfirmCodeModal);
}

export function* signOutUser() {
  yield takeEvery(SIGNOUT_USER, signOut);
}

export function* userForgotPassword() {
  yield takeEvery(FORGOT_PASSWORD, forgotPassword);
}

export function* userForgotPasswordSubmit() {
  yield takeEvery(FORGOT_PASSWORD_SUBMIT, forgotPasswordSubmit);
}


export default function* rootSaga() {
  yield all([
    fork(signInUser),
    fork(createUserAccount),
    fork(enterConfCode),
    fork(signOutUser),
    fork(userForgotPassword),
    fork(userForgotPasswordSubmit)
  ]);
}
