/**
* File Modified
*/
import {all} from 'redux-saga/effects';
import authSagas from './Auth';
import WindowDesignerSagas from './WindowDesigner';

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    WindowDesignerSagas(),
  ]);
}
