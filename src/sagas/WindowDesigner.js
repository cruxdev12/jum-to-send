/**
 * File Created for RYBW
 */
// eslint-disable-next-line
import {all, call, fork, put, takeEvery} from 'redux-saga/effects';
import { SAVE_CAMPAIGN_POST, SAVE_CAMPAIGN_POST_IMG, LOAD_SAVED_CAMPAIGNS } from '../constants/ActionTypes';

import { API, Storage, graphqlOperation } from 'aws-amplify';
import { createCampaignPost, createCampaignPostContent } from '../graphql/mutations';
import { listCampaignPostContents } from '../graphql/queries';

import { showLoader, hideLoader, showNotification, showErrorMsg, showSuccessMsg, campaignsModalAction, savedCampaignsDataAction } from '../actions/WindowDesigner';

// create Campaign ID
// function createUUID() {
//   let dt = new Date().getTime();
//   const my = `${new Date().getFullYear()}${new Date().getMonth() + 1}`;
//   const uuid = `campAu-${my}${'-yxxxxx'.replace(/[xy]/g, (c) => {
//     const r = (dt + Math.random() * 16) % 16 | 0;
//     dt = Math.floor(dt / 16);
//     return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
//   })}`;
//   return uuid;
// }

// Workers Saga
function* loadSavedCampaignsWorker() {
  console.log('IN loadSavedCampaignsWorker');
  yield put(campaignsModalAction(true));

  try {
    const data = yield API.graphql(graphqlOperation(listCampaignPostContents));
    yield put(savedCampaignsDataAction(data));
  } catch (error) {
    console.log('listCampaignPostContents ERROR: ', error);
  }

}

function* saveCampaignPostWorker({payload}) {
  console.log('IN saveCampaignPostWorker');

  try {
    if (!payload.jpegImg || !payload.svgData) { // for empty image
      const jpgError = 'image error, please try again';
      throw jpgError;
    }
    // IMAGES
    const buf = new Buffer(payload.jpegImg.replace(/^data:image\/\w+;base64,/, ''), 'base64');
    // const data = {
    //   Body: buf,
    //   ContentEncoding: 'base64',
    //   ContentType: 'image/jpeg'
    // };
    const svgSW = new Blob([payload.svgData[0]], {type: 'image/svg+xml;charset=utf-8'});
    const svgMW = new Blob([payload.svgData[1]], {type: 'image/svg+xml;charset=utf-8'});
    const svgLW = new Blob([payload.svgData[2]], {type: 'image/svg+xml;charset=utf-8'});

    // DB
    const campPostData = {
      id: payload.id,
      campaignName: payload.campName,
      startDate: payload.campDate,
      duration: payload.campDuration,
      approvedDate: 'not appoved',
      campaignLocation: payload.campArea,
      carsToBeDeployed: payload.campCars,
      CAMPAIGNSTATUS: 'PENDING'
    };
    let campContentData; // for empty comment
    if (!payload.artworkComment) {
      campContentData = {
        id: payload.id,
        CONTENTSTATUS: 'PENDING',
        artworkFile: payload.artworkFile,
      };
    } else {
      campContentData = {
        id: payload.id,
        CONTENTSTATUS: 'PENDING',
        artworkFile: payload.artworkFile,
        artworkComment: payload.artworkComment
      };
    }

    // const campContentData = {
    //   campaignsUsed: [campId],
    //   CONTENTSTATUS: 'PENDING',
    //   artworkFile: payload.artworkFile,
    //   artworkComment: payload.artworkComment
    // };

    yield put(showLoader());
    yield put(showNotification(true));

    yield Storage.put(`campaignsContent/${payload.id}/artwork.jpeg`, buf, {
      level: 'private',
      ContentEncoding: 'base64',
      ContentType: 'image/jpeg'
    });
    yield Storage.put(`campaignsContent/${payload.id}/printFiles/smallBW.svg`, svgSW, {
      level: 'private',
      ContentType: 'image/svg+xml'
    });
    yield Storage.put(`campaignsContent/${payload.id}/printFiles/mediumBW.svg`, svgMW, {
      level: 'private',
      ContentType: 'image/svg+xml'
    });
    yield Storage.put(`campaignsContent/${payload.id}/printFiles/largeBW.svg`, svgLW, {
      level: 'private',
      ContentType: 'image/svg+xml'
    });
    yield API.graphql(graphqlOperation(createCampaignPost, {input: campPostData}));
    yield API.graphql(graphqlOperation(createCampaignPostContent, {input: campContentData}));

    yield put(showSuccessMsg('Campaign has been saved!'));
    yield put(hideLoader());
    yield put(showNotification(false));

  } catch (error) {
    yield put(hideLoader());
    yield put(showErrorMsg('Something went wrong, please try again!'));
    yield put(showNotification(false));
    console.log('SOMETHING WENT WRONG', error);
  }

}


function* saveCampaignPostIMGWorker({payload}) {
  console.log('IN saveCampaignPostIMGWorker');
  try {
    if (!payload.jpegImg || !payload.svgData) { // for empty image
      const jpgError = 'image error, please try again';
      throw jpgError;
    }
    const buf = new Buffer(payload.jpegImg.replace(/^data:image\/\w+;base64,/, ''), 'base64');
    // const data = {
    //   Body: buf,
    //   ContentEncoding: 'base64',
    //   ContentType: 'image/jpeg'
    // };
    const svgSW = new Blob([payload.svgData[0]], {type: 'image/svg+xml;charset=utf-8'});
    const svgMW = new Blob([payload.svgData[1]], {type: 'image/svg+xml;charset=utf-8'});
    const svgLW = new Blob([payload.svgData[2]], {type: 'image/svg+xml;charset=utf-8'});

    yield Storage.put(`campaignsContent/${payload.id}/artwork.jpeg`, buf, {
      level: 'private',
      ContentEncoding: 'base64',
      ContentType: 'image/jpeg'
    });
    yield Storage.put(`campaignsContent/${payload.id}/printFiles/smallBW.svg`, svgSW, {
      level: 'private',
      ContentType: 'image/svg+xml'
    });
    yield Storage.put(`campaignsContent/${payload.id}/printFiles/mediumBW.svg`, svgMW, {
      level: 'private',
      ContentType: 'image/svg+xml'
    });
    yield Storage.put(`campaignsContent/${payload.id}/printFiles/largeBW.svg`, svgLW, {
      level: 'private',
      ContentType: 'image/svg+xml'
    });

  } catch (err) {
    yield put(hideLoader());
    yield put(showErrorMsg('Something went wrong, please try again!'));
    yield put(showNotification(false));
    // console.log('SOMETHING WENT WRONG', error);
  }
}

// Watchers Saga
export function* saveCampaignPost() {
  yield takeEvery(SAVE_CAMPAIGN_POST, saveCampaignPostWorker);
}

export function* saveCampaignPostImg() {
  yield takeEvery(SAVE_CAMPAIGN_POST_IMG, saveCampaignPostIMGWorker);
}

export function* loadSavedCampaigns() {
  yield takeEvery(LOAD_SAVED_CAMPAIGNS, loadSavedCampaignsWorker);
}

export default function* rootSaga() {
  yield all([
    fork(saveCampaignPost),
    fork(saveCampaignPostImg),
    fork(loadSavedCampaigns)
  ]);
}
