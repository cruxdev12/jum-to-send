/* config-overrides.js */

const dev = require('./config/webpack/Dev');
const dist = require('./config/webpack/Dist');

module.exports = {
  dev,
  dist
};
